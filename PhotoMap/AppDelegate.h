//
//  AppDelegate.h
//  PhotoMap
//
//  Created by mac-251 on 12/17/15.
//  Copyright © 2015 mac-251. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GCNetworkReachability/GCNetworkReachability.h>

@class RACSignal;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

+ (AppDelegate*)sharedDelegate;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) GCNetworkReachability *reachibility;

- (void)setupRootViewControllerAnimated:(BOOL)animated;
- (RACSignal*)reachableSignal;

@end

