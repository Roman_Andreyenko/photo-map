//
//  UIColor+AppColors.m
//  PhotoMap
//
//  Created by mac-251 on 3/21/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "UIColor+AppColors.h"

@implementation UIColor (AppColors)

+ (UIColor*)natureCategoryColor {
    return [UIColor colorWithRed:87.f / 255.f green:142.f / 255.f blue:24.f / 255.f alpha:1.f];
}

+ (UIColor*)friendsCategoryColor {
    return [UIColor colorWithRed:244.f / 255.f green:165.f / 255.f blue:35.f / 255.f alpha:1.f];
}

+ (UIColor*)defaultCategoryColor {
    return [UIColor colorWithRed:54.f / 255.f green:142.f / 255.f blue:223.f / 255.f alpha:1.f];
}

@end
