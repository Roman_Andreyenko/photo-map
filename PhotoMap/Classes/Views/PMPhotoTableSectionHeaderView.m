//
//  PMPhotoTableSectionHeaderView.m
//  PhotoMap
//
//  Created by mac-251 on 3/30/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMPhotoTableSectionHeaderView.h"
#import "PMPhoto.h"
#import "PMDateManager.h"

@implementation PMPhotoTableSectionHeaderView

- (void)updateViewWithPhoto:(PMPhoto*)photo {
    self.headerView.text = [[PMDateManager defaultDateManager].fullMonthYearDateFormatter stringFromDate:photo.createdTime];
}

@end
