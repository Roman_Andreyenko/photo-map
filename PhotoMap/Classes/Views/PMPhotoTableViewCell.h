//
//  PMPhotoTableViewCell.h
//  PhotoMap
//
//  Created by mac-251 on 3/22/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PMPhoto;

@interface PMPhotoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *descriptionTextView;
@property (weak, nonatomic) IBOutlet UILabel *dateAndCategoryView;
@property (weak, nonatomic) IBOutlet UIImageView *photoView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

- (void)updateCellWithPhoto:(PMPhoto*)photo;

@end
