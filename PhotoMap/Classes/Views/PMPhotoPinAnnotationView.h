//
//  PMPhotoPinAnnotationView.h
//  PhotoMap
//
//  Created by mac-251 on 3/16/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface PMPhotoPinAnnotationView : MKAnnotationView

- (void)updateViewWithAnnotation:(id <MKAnnotation>)annotation;

@end
