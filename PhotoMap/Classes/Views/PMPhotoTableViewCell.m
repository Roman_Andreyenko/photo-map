//
//  PMPhotoTableViewCell.m
//  PhotoMap
//
//  Created by mac-251 on 3/22/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMPhotoTableViewCell.h"
#import "PMPhoto.h"
#import "PMDateManager.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@implementation PMPhotoTableViewCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCellWithPhoto:(PMPhoto*)photo {
    self.descriptionTextView.hidden = !photo.descriptionText || [@"" isEqualToString:photo.descriptionText];
    self.descriptionTextView.text = photo.descriptionText;
    self.dateAndCategoryView.text = [NSString stringWithFormat:@"%@ / %@", [[PMDateManager defaultDateManager].shortDateFormatter stringFromDate:photo.createdTime], [photo.category uppercaseString]];
    self.activityIndicatorView.hidden = NO;
    self.photoView.image = nil;
    [self.activityIndicatorView startAnimating];
    @weakify(self)
    [[photo loadPhotoThumbnailDataSignalTakeUntil:[self rac_prepareForReuseSignal]] subscribeNext:^(UIImage* image) {
        @strongify(self)
        self.photoView.image = image;
    } error:^(NSError *error) {
        @strongify(self)
        [self.activityIndicatorView stopAnimating];
        self.activityIndicatorView.hidden = YES;
    } completed:^{
        @strongify(self)
        [self.activityIndicatorView stopAnimating];
        self.activityIndicatorView.hidden = YES;
    }];
}

@end
