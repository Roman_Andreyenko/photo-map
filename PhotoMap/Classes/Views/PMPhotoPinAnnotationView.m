//
//  PMPhotoPinAnnotationView.m
//  PhotoMap
//
//  Created by mac-251 on 3/16/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMPhotoPinAnnotationView.h"
#import "PMPhotoAnnotation.h"
#import "PMPhoto.h"

CGFloat const kCalloutAccessoryViewWidth = 55.f;
CGFloat const kCalloutAccessoryViewHeight = 55.f;

@implementation PMPhotoPinAnnotationView

- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier]) {
        self.canShowCallout = YES;
        UIImageView *photoView = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, kCalloutAccessoryViewWidth, kCalloutAccessoryViewHeight)];
        photoView.contentMode = UIViewContentModeScaleAspectFit;
        self.leftCalloutAccessoryView = photoView;
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [rightButton setImage:[UIImage imageNamed:@"ArrowRightIcon"] forState:UIControlStateNormal];
        [rightButton sizeToFit];
        rightButton.userInteractionEnabled = NO;
        self.rightCalloutAccessoryView = rightButton;
        [self updateViewWithAnnotation:annotation];
    }
    
    return self;
}

- (void)updateViewWithAnnotation:(id <MKAnnotation>)annotation {
    self.annotation = annotation;
    PMPhotoAnnotation *photoAnnotation = annotation;
    self.image = [PMPhoto imageIconForCategory:photoAnnotation.photo.category];
    self.calloutOffset = CGPointMake(0.f, self.image.size.height / 2.f);    
}

@end
