//
//  PMPhotoTableSectionHeaderView.h
//  PhotoMap
//
//  Created by mac-251 on 3/30/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PMPhoto;

@interface PMPhotoTableSectionHeaderView : UITableViewHeaderFooterView

@property (weak, nonatomic) IBOutlet UILabel *headerView;

- (void)updateViewWithPhoto:(PMPhoto*)photo;

@end
