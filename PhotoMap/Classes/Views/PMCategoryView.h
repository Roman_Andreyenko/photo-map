//
//  PMCategoryView.h
//  PhotoMap
//
//  Created by mac-251 on 2/29/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMCategoryView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *categoryIconView;
@property (weak, nonatomic) IBOutlet UILabel *categoryTextView;

@end
