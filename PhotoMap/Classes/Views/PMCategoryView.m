//
//  PMCategoryView.m
//  PhotoMap
//
//  Created by mac-251 on 2/29/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMCategoryView.h"

@implementation PMCategoryView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initialize];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self initialize];
    }
    
    return self;
}

- (void)initialize {
    UIView *view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([PMCategoryView class]) owner:self options:nil] firstObject];
    view.frame = self.frame;
    [self addSubview:view];
}

@end
