//
//  PMPhotoHashtag.h
//  PhotoMap
//
//  Created by mac-251 on 2/24/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <Parse/Parse.h>

@class PMPhoto;
@class PMHashtag;

@interface PMPhotoIndex : PFObject<PFSubclassing>

+ (instancetype)photoIndexWithPhoto:(PMPhoto*)photo hashtag:(PMHashtag*)hashtag;

@property (nonatomic, strong) PMPhoto *photo;
@property (nonatomic, strong) PMHashtag *hashtag;

@end
