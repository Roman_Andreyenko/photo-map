//
//  PMPhotoSectionWrapper.m
//  PhotoMap
//
//  Created by mac-251 on 3/24/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMPhotoSectionWrapper.h"

@implementation PMPhotoSectionWrapper

- (instancetype)init {
    if (self = [super init]) {
        self.startIndex = 0;
        self.size = 0;
    }
    return self;
}

- (instancetype)initWithStartIndex:(NSInteger)startIndex size:(NSInteger)size {
    if (self = [super init]) {
        self.startIndex = startIndex;
        self.size = size;
    }
    return self;
}

@end
