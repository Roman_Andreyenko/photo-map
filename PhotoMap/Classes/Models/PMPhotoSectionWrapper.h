//
//  PMPhotoSectionWrapper.h
//  PhotoMap
//
//  Created by mac-251 on 3/24/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMPhotoSectionWrapper : NSObject

- (instancetype)initWithStartIndex:(NSInteger)startIndex size:(NSInteger)size;

@property (nonatomic, assign) NSInteger startIndex;
@property (nonatomic, assign) NSInteger size;

@end
