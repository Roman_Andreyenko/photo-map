//
//  PMPhoto.h
//  PhotoMap
//
//  Created by mac-251 on 2/24/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <Parse/Parse.h>

FOUNDATION_EXPORT NSString *const kDefaultCategory;
FOUNDATION_EXPORT NSString *const kFriendsCategory;
FOUNDATION_EXPORT NSString *const kNatureCategory;

@class RACSignal;

@interface PMPhoto : PFObject<PFSubclassing>

+ (instancetype)photoWithData:(NSData*)photoData thumbnailData:(NSData*)photoThumbnailData createdTime:(NSDate*)createdDate category:(NSString*)category descriptionText:(NSString*)descriptionText latitude:(double)latitude longitude:(double)longitude owner:(PFUser*)owner error:(NSError **)error;
+ (UIImage*)imageIconForCategory:(NSString*)category;

- (RACSignal*)loadPhotoDataSignalTakeUntil:(RACSignal*)takeUntilSignal;
- (RACSignal*)loadPhotoThumbnailDataSignalTakeUntil:(RACSignal*)takeUntilSignal;

@property (nonatomic, strong) PFUser *owner;
@property (nonatomic, strong) PFFile *imageFile;
@property (nonatomic, strong) PFFile *imageThumbnailFile;
@property (nonatomic, strong) NSDate *createdTime;
@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *descriptionText;
@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;

@end
