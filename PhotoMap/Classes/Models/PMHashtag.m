//
//  PMHashtag.m
//  PhotoMap
//
//  Created by mac-251 on 2/24/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMHashtag.h"

@implementation PMHashtag

@dynamic hashtag;

+ (void)load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return NSStringFromClass([PMHashtag class]);
}

+ (instancetype)hashtagWithTag:(NSString*)hashtag {
    PMHashtag *tag = [PMHashtag object];
    tag.hashtag = hashtag;
    
    return tag;
}

+ (NSArray*)parseTagsInText:(NSString*)textWithTags {
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"#(\\w+)" options:0 error:&error];
    NSArray *matches = [regex matchesInString:textWithTags options:0 range:NSMakeRange(0, textWithTags.length)];
    NSMutableSet *hashtags = [NSMutableSet setWithCapacity:matches.count];
    
    for (NSTextCheckingResult *match in matches) {
        NSRange wordRange = [match rangeAtIndex:1];
        NSString* word = [textWithTags substringWithRange:wordRange];
        [hashtags addObject:word];
    }
    
    return [hashtags allObjects];
}

@end
