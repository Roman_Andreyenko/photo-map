//
//  PMPhotoAnnotation.m
//  PhotoMap
//
//  Created by mac-251 on 3/15/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMPhotoAnnotation.h"
#import "PMPhoto.h"

@interface PMPhotoAnnotation ()

@property (nonatomic, strong) NSDateFormatter *subtitleDateFormatter;

@end

@implementation PMPhotoAnnotation

- (instancetype)initWithPhoto:(PMPhoto*)photo {
    if (self = [super init]) {
        self.photo = photo;
        self.subtitleDateFormatter = [NSDateFormatter new];
        self.subtitleDateFormatter.dateFormat = @"MM-dd-yyyy";
    }
    return self;
}

- (CLLocationCoordinate2D)coordinate {
    return CLLocationCoordinate2DMake([self.photo.latitude doubleValue], [self.photo.longitude doubleValue]);
}

- (NSString*)title {
    return self.photo.descriptionText != nil && self.photo.descriptionText.length > 0 ? self.photo.descriptionText : @" ";
}

- (NSString*)subtitle {
    return [self.subtitleDateFormatter stringFromDate:self.photo.createdTime];
}

@end
