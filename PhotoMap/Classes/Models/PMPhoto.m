//
//  PMPhoto.m
//  PhotoMap
//
//  Created by mac-251 on 2/24/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMPhoto.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

NSString *const kDefaultCategory = @"Default";
NSString *const kFriendsCategory = @"Friends";
NSString *const kNatureCategory = @"Nature";

@implementation PMPhoto

@dynamic owner;
@dynamic imageFile;
@dynamic imageThumbnailFile;
@dynamic createdTime;
@dynamic category;
@dynamic descriptionText;
@dynamic latitude;
@dynamic longitude;

+ (void)load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return NSStringFromClass([PMPhoto class]);
}

+ (instancetype)photoWithData:(NSData*)photoData thumbnailData:(NSData*)photoThumbnailData createdTime:(NSDate*)createdDate category:(NSString*)category descriptionText:(NSString*)descriptionText latitude:(double)latitude longitude:(double)longitude  owner:(PFUser*)owner error:(NSError **)error {
    PMPhoto* photo = [PMPhoto object];
    photo.owner = owner;
    photo.imageFile = [PFFile fileWithName:nil data:photoData contentType:nil error:error];
    photo.imageThumbnailFile = [PFFile fileWithData:photoThumbnailData];
    photo.createdTime = createdDate;
    photo.category = category;
    photo.descriptionText = descriptionText;
    photo.latitude = [NSNumber numberWithDouble:latitude];
    photo.longitude = [NSNumber numberWithDouble:longitude];
    
    return photo;
}

+ (UIImage*)imageIconForCategory:(NSString*)category {
    if ([kFriendsCategory isEqualToString:category]) {
        return [UIImage imageNamed:@"MarkerFriendsIcon"];
    } else if ([kNatureCategory isEqualToString:category]) {
        return [UIImage imageNamed:@"MarkerNatureIcon"];
    } else {
        return [UIImage imageNamed:@"MarkerDefaultIcon"];
    }
}

- (RACSignal*)loadPhotoDataSignalTakeUntil:(RACSignal*)takeUntilSignal {
    return [self loadPicture:self.imageFile takeUntil:takeUntilSignal];
}

- (RACSignal*)loadPhotoThumbnailDataSignalTakeUntil:(RACSignal*)takeUntilSignal {
    return [self loadPicture:self.imageThumbnailFile takeUntil:takeUntilSignal];
}

- (RACSignal*)loadPicture:(PFFile*)picturteFile takeUntil:(RACSignal*)takeUntilSignal {
    return [[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [picturteFile getDataInBackgroundWithBlock:^(NSData * _Nullable data, NSError * _Nullable error) {
            if (error) {
                [subscriber sendError:error];
            } else {
                [subscriber sendNext:[UIImage imageWithData:data]];
                [subscriber sendCompleted];
            }
        }];
        return nil;
    }] takeUntil:takeUntilSignal];
}

@end
