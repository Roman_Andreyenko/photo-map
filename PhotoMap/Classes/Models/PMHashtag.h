//
//  PMHashtag.h
//  PhotoMap
//
//  Created by mac-251 on 2/24/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <Parse/Parse.h>

@interface PMHashtag : PFObject<PFSubclassing>

+ (instancetype)hashtagWithTag:(NSString*)hashtag;
+ (NSArray*)parseTagsInText:(NSString*)textWithTags;

@property (nonatomic, strong) NSString *hashtag;

@end
