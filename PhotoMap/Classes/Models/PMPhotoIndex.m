//
//  PMPhotoHashtag.m
//  PhotoMap
//
//  Created by mac-251 on 2/24/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMPhotoIndex.h"
#import "PMPhoto.h"
#import "PMHashtag.h"

@implementation PMPhotoIndex

@dynamic photo;
@dynamic hashtag;

+ (void)load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return NSStringFromClass([PMPhotoIndex class]);
}

+ (instancetype)photoIndexWithPhoto:(PMPhoto*)photo hashtag:(PMHashtag*)hashtag {
    PMPhotoIndex *photoIndex = [PMPhotoIndex object];
    photoIndex.photo = photo;
    photoIndex.hashtag = hashtag;
    
    return photoIndex;
}

@end
