//
//  PMPhotoAnnotation.h
//  PhotoMap
//
//  Created by mac-251 on 3/15/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@class PMPhoto;

@interface PMPhotoAnnotation : NSObject <MKAnnotation>

- (instancetype)initWithPhoto:(PMPhoto*)photo;

@property (nonatomic, strong) PMPhoto *photo;

@end
