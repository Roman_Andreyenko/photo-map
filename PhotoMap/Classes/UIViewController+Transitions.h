//
//  UIViewController+TransitionsViewController.h
//  PhotoMap
//
//  Created by mac-251 on 3/30/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Transitions)

- (void)presentHorizontallyViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion;
- (void)dismissHorizontallyViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion;

@end
