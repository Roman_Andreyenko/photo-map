//
//  UIColor+AppColors.h
//  PhotoMap
//
//  Created by mac-251 on 3/21/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (AppColors)

+ (UIColor*)natureCategoryColor;
+ (UIColor*)friendsCategoryColor;
+ (UIColor*)defaultCategoryColor;

@end
