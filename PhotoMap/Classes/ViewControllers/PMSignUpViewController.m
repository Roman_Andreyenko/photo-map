//
//  PMSignUpViewController.m
//  PhotoMap
//
//  Created by mac-251 on 2/12/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMSignUpViewController.h"
#import "PMSignUpViewModel.h"
#import "AppDelegate.h"
#import "PMUtils.h"
#import <Parse/Parse.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface PMSignUpViewController ()

- (void)bindViewModel;

@property (weak, nonatomic) PMSignUpViewModel *viewModel;

@end

@implementation PMSignUpViewController

- (instancetype)initWithViewModel:(PMSignUpViewModel *)viewModel {
    self = [super init];
    if (self ) {
        _viewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self bindViewModel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)bindViewModel {
    self.usernameView.delegate = self;
    self.emailView.delegate = self;
    self.passwordView.delegate = self;
    self.confirmPasswordView.delegate = self;
    RAC(self.viewModel.potentialUser, username) = self.usernameView.rac_textSignal;
    RAC(self.viewModel.potentialUser, email) = self.emailView.rac_textSignal;
    RAC(self.viewModel.potentialUser, password) = self.passwordView.rac_textSignal;
    RAC(self.viewModel, confirmPasswordText) = self.confirmPasswordView.rac_textSignal;
    self.signUpView.rac_command = self.viewModel.executeSignUp;
    RAC(self.activityIndicatorView, hidden) = [self.viewModel.executeSignUp.executing not];
    @weakify(self)
    [[self.viewModel.executeSignUp.executionSignals switchToLatest] subscribeNext:^(NSNumber *result) {
        @strongify(self)
        [self hideKeyboard];
        [[AppDelegate sharedDelegate] setupRootViewControllerAnimated:YES];
    }];
    [self.viewModel.executeSignUp.errors subscribeNext:^(NSError *error) {
        @strongify(self)
        [PMUtils controller:self showAlertWithMessage:[error localizedDescription]];
        [self hideKeyboard];
    }];
}

- (void)hideKeyboard {
    [self.usernameView resignFirstResponder];
    [self.emailView resignFirstResponder];
    [self.passwordView resignFirstResponder];
    [self.confirmPasswordView resignFirstResponder];
}

- (void)setValidationView:(UILabel*)validationView forValidationSignal:(RACSignal*)validationSignal observedView:(UITextField *)observedView forField:(UITextField*)textField {
    if (textField == observedView) {
        RAC(validationView, text) = validationSignal;
        observedView.delegate = nil;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    [self setValidationView:self.usernameValidationView forValidationSignal:self.viewModel.validUsernameSignal observedView:self.usernameView forField:textField];
    [self setValidationView:self.emailValidationView forValidationSignal:self.viewModel.validEmailSignal observedView:self.emailView forField:textField];
    [self setValidationView:self.passwordValidationView forValidationSignal:self.viewModel.validPasswordSignal observedView:self.passwordView forField:textField];
    [self setValidationView:self.confirmPasswordValidationView forValidationSignal:self.viewModel.validConfirmPasswordSignal observedView:self.confirmPasswordView forField:textField];
    
    return YES;
}

@end
