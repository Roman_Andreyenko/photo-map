//
//  PMLogInViewController.m
//  PhotoMap
//
//  Created by mac-251 on 2/12/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMLogInViewController.h"
#import "PMLogInViewModel.h"
#import "AppDelegate.h"
#import "PMUtils.h"
#import <Parse/Parse.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface PMLogInViewController ()

- (void)bindViewModel;

@property (weak, nonatomic) PMLogInViewModel *viewModel;

@end

@implementation PMLogInViewController

- (instancetype)initWithViewModel:(PMLogInViewModel *)viewModel {
    self = [super init];
    if (self ) {
        _viewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self bindViewModel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)bindViewModel {
    self.usernameView.delegate = self;
    self.passwordView.delegate = self;
    RAC(self.viewModel, usernameText) = self.usernameView.rac_textSignal;
    RAC(self.viewModel, passwordText) = self.passwordView.rac_textSignal;
    self.logInView.rac_command = self.viewModel.executeLogIn;
    RAC(self.activityIndicatorView, hidden) = [self.viewModel.executeLogIn.executing not];
    @weakify(self)
    [[self.viewModel.executeLogIn.executionSignals switchToLatest] subscribeNext:^(PFUser *user) {
        @strongify(self)
        [self hideKeyboard];
        [[AppDelegate sharedDelegate] setupRootViewControllerAnimated:YES];
    }];
    [self.viewModel.executeLogIn.errors subscribeNext:^(NSError *error) {
        @strongify(self)
        [PMUtils controller:self showAlertWithMessage:[error localizedDescription]];
        [self hideKeyboard];
    }];
}

- (void)hideKeyboard {
    [self.usernameView resignFirstResponder];
    [self.passwordView resignFirstResponder];
}

- (void)setValidationView:(UILabel*)validationView forValidationSignal:(RACSignal*)validationSignal observedView:(UITextField *)observedView forField:(UITextField*)textField {
    if (textField == observedView) {
        RAC(validationView, text) = validationSignal;
        observedView.delegate = nil;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    [self setValidationView:self.usernameValidationView forValidationSignal:self.viewModel.validUsernameSignal observedView:self.usernameView forField:textField];
    [self setValidationView:self.passwordValidationView forValidationSignal:self.viewModel.validPasswordSignal observedView:self.passwordView forField:textField];
    
    return YES;
}

@end
