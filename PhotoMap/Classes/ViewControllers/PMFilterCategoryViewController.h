//
//  PMFilterCategoryViewController.h
//  PhotoMap
//
//  Created by mac-251 on 3/18/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PMFilterCategoryViewModel;

@interface PMFilterCategoryViewController : UIViewController

- (instancetype)initWithViewModel:(PMFilterCategoryViewModel *)viewModel;

@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UIButton *natureCategoryView;
@property (weak, nonatomic) IBOutlet UIButton *friendsCategoryView;
@property (weak, nonatomic) IBOutlet UIButton *defaultCategoryView;


@end
