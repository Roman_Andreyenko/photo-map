//
//  PMMapViewController.m
//  PhotoMap
//
//  Created by mac-251 on 1/6/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMMapViewController.h"
#import "PMMapViewModel.h"
#import "PMFilterCategoryViewModel.h"
#import "PMCategoryView.h"
#import "PMPhoto.h"
#import "PMPhotoAnnotation.h"
#import "PMPhotoPinAnnotationView.h"
#import "PMUtils.h"
#import "PMFilterCategoryViewController.h"
#import "PMFullPhotoViewController.h"
#import "UIViewController+Transitions.h"
#import "PMFullPhotoViewModel.h"
#import "AppDelegate.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

CGFloat const kPickerViewCellHeight = 56.f;

@interface PMMapViewController ()

- (void)bindViewModel;

@property (strong, nonatomic) PMFilterCategoryViewModel *filterViewModel;
@property (strong, nonatomic) PMFullPhotoViewModel *fullPhotoViewModel;
@property (weak, nonatomic) PMMapViewModel *viewModel;
@property (strong, nonatomic) UIImagePickerController *imagePicker;

@end

@implementation PMMapViewController

- (instancetype)initWithViewModel:(PMMapViewModel *)viewModel {
    self = [super init];
    if (self ) {
        _viewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self configureSubviews];
    
    self.createPhotoDescriptionTextView.delegate = self;
    
    [self bindViewModel];

    self.imagePicker.delegate = self;
    self.categoryPickerView.delegate = self;
    self.categoryPickerView.dataSource = self;
    self.mapView.delegate = self;
    
    [self.viewModel requestUserLocationAuthorization];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureSubviews {
    self.imagePicker = [UIImagePickerController new];
    self.mapView.userTrackingMode = MKUserTrackingModeFollow;
    self.createPhotoDescriptionTextView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.createPhotoDescriptionTextView.layer.borderWidth = 2.f;
    self.createPhotoPopupView.layer.cornerRadius = 5.f;
    [self setupShadowForView:self.createPhotoPopupView withOffsetSize:CGSizeMake(3.f, 3.f)];
    [self setupShadowForView:self.pickedPhotoView withOffsetSize:CGSizeMake(-3.f, 3.f)];
}

- (void)setupShadowForView:(UIView*)view withOffsetSize:(CGSize)offsetSize {
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = offsetSize;
    view.layer.shadowRadius = 3.f;
    view.layer.shadowOpacity = 0.5f;
    view.layer.shadowColor = [[UIColor blackColor] CGColor];
}

- (void)openFullPhotoScreenWithPhoto:(PMPhoto*)photo {
    self.fullPhotoViewModel = [[PMFullPhotoViewModel alloc] initWithPhoto:photo];
    PMFullPhotoViewController *fullPhotoViewController = [[PMFullPhotoViewController alloc] initWithViewModel:self.fullPhotoViewModel];
    [self presentHorizontallyViewController:fullPhotoViewController animated:NO completion:nil];
}

- (void)bindViewModel {
    [self bindActivityExecuting];
    [self bindFullPhotoAppearing];
    [self bindKeyboardAppearing];
    [self bindNetworkAvailability];
    [self bindMapNavigationModeViewModel];
    [self bindFilteredCategoriesViewModel];
    [self bindPhotoLocationViewModel];
    [self bindSelectedPhotoCategoryViewModel];
    [self bindPhotoCreationViewModel];
    [self bindPinPhotoLoadingViewModel];
}

- (void)bindFullPhotoAppearing {
    @weakify(self)
    [self.pickedPhotoViewTapGestureRecognizer.rac_gestureSignal subscribeNext:^(id x) {
        @strongify(self)
        [self openFullPhotoScreenWithPhoto:[self.viewModel photoWithPhotoData]];
    }];
    [self.selectedPinTapGestureRecognizer.rac_gestureSignal subscribeNext:^(UITapGestureRecognizer *recognizer) {
        @strongify(self)
        PMPhotoPinAnnotationView *pinAnnotationView = (PMPhotoPinAnnotationView *)recognizer.view;
        PMPhotoAnnotation *photoAnnotation = pinAnnotationView.annotation;
        [self openFullPhotoScreenWithPhoto:photoAnnotation.photo];
    }];
    [[[self rac_signalForSelector:@selector(mapView:didDeselectAnnotationView:)] filter:^BOOL(RACTuple *tuple) {
        MKAnnotationView *annotationView = tuple.second;
        return [annotationView.annotation isKindOfClass:[PMPhotoAnnotation class]];
    }] subscribeNext:^(RACTuple *tuple) {
        @strongify(self)
        PMPhotoPinAnnotationView *photoView = tuple.second;
        [photoView removeGestureRecognizer:self.selectedPinTapGestureRecognizer];
    }];
}

- (void)bindPinPhotoLoadingViewModel {
    __block PMPhotoPinAnnotationView *pinAnnotationView = nil;
    @weakify(self)
    RACSignal* selectAnnotationViewSignal = [self rac_signalForSelector:@selector(mapView:didSelectAnnotationView:) fromProtocol:@protocol(MKMapViewDelegate)];
    [[[selectAnnotationViewSignal filter:^BOOL(RACTuple *tuple) {
        MKAnnotationView *annotationView = tuple.second;
        return [annotationView.annotation isKindOfClass:[PMPhotoAnnotation class]];
    }] flattenMap:^RACStream *(RACTuple *tuple) {
        @strongify(self)
        pinAnnotationView = tuple.second;
        UIImageView *leftView = (UIImageView*)pinAnnotationView.leftCalloutAccessoryView;
        leftView.image = [UIImage imageNamed:@"PlaceholderIcon"];
        PMPhotoAnnotation *photoAnnotation = pinAnnotationView.annotation;
        MKCoordinateRegion region = self.mapView.region;
        region.center.latitude = [photoAnnotation.photo.latitude doubleValue];
        region.center.longitude = [photoAnnotation.photo.longitude doubleValue];
        [self.mapView setRegion:region animated:YES];
        [pinAnnotationView addGestureRecognizer:self.selectedPinTapGestureRecognizer];
        return [self.viewModel loadPhotoThumbnailDataSignalWithPhoto:photoAnnotation.photo takeUntil:selectAnnotationViewSignal];
    }] subscribeNext:^(UIImage *photoImage) {
        UIImageView *leftView = (UIImageView*)pinAnnotationView.leftCalloutAccessoryView;
        leftView.image = photoImage;
    } error:^(NSError *error) {
        @strongify(self)
        [PMUtils controller:self showAlertWithMessage:[error localizedDescription]];
    }];
}

- (void)bindActivityExecuting {
    RACSignal *executingActivitySignal = [RACSignal merge:@[[self.viewModel.executeCreatePhoto.executing not], [self.viewModel.executeLoadPhotos.executing not]]];
    RAC(self.activityIndicatorView, hidden) = executingActivitySignal;
    RAC(self.view, userInteractionEnabled) = executingActivitySignal;
}

- (void)bindPhotoCreationViewModel {
    @weakify(self)
    RAC(self.viewModel, photoDescriptionText) = [RACSignal merge:@[self.createPhotoDescriptionTextView.rac_textSignal, RACObserve(self.createPhotoDescriptionTextView, text)]];
    [[self rac_signalForSelector:@selector(imagePickerController:didFinishPickingMediaWithInfo:)] subscribeNext:^(RACTuple *tuple) {
        @strongify(self)
        RACTupleUnpack(UIImagePickerController *picker, NSDictionary *info) = tuple;
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        [self.viewModel setPhotoDataWithImage:image];
        self.viewModel.photoCreatedDate = [NSDate new];
        self.createPhotoPopupOverlayView.hidden = NO;
        self.pickedPhotoView.image = image;
        self.pickedPhotoCreatedDateView.text = [self.viewModel formatPhotoCreatedDate];
        self.createPhotoDescriptionTextView.text = @"";
        [self.categoryPickerView selectRow:0 inComponent:0 animated:NO];
        [self setupSelectedCategory:[self.viewModel.categories objectAtIndex:0]];
        [picker dismissViewControllerAnimated:YES completion:nil];
    }];
    [[self.createPhotoCancelView rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        [self.createPhotoDescriptionTextView resignFirstResponder];
        self.createPhotoPopupOverlayView.hidden = YES;
        [self.viewModel clearPhotoData];
    }];
    self.createPhotoDoneView.rac_command = self.viewModel.executeCreatePhoto;
    [[self.viewModel.executeCreatePhoto.executionSignals switchToLatest] subscribeNext:^(PMPhotoAnnotation *createdPhotoAnnotation) {
        @strongify(self)
        [self.createPhotoDescriptionTextView resignFirstResponder];
        self.createPhotoPopupOverlayView.hidden = YES;
        [self.mapView addAnnotation:createdPhotoAnnotation];
    }];
    [self.viewModel.executeCreatePhoto.errors subscribeNext:^(NSError *error) {
        @strongify(self)
        [self.createPhotoDescriptionTextView resignFirstResponder];
        self.createPhotoPopupOverlayView.hidden = YES;
        [PMUtils controller:self showAlertWithMessage:[error localizedDescription]];
    }];
}

- (void)bindSelectedPhotoCategoryViewModel {
    @weakify(self);
    [[self.selectedCategoryView rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        self.categoryPickerOverlay.hidden = NO;
    }];
    [[self rac_signalForSelector:@selector(pickerView:didSelectRow:inComponent:) fromProtocol:@protocol(UIPickerViewDelegate)] subscribeNext:^(RACTuple *tuple) {
        @strongify(self)
        NSInteger row = [tuple.second integerValue];
        [self setupSelectedCategory:[self.viewModel.categories objectAtIndex:row]];
    }];
    [[self.categoryPickerDoneView rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        self.categoryPickerOverlay.hidden = YES;
    }];
}

- (void)bindMapNavigationModeViewModel {
    @weakify(self);
    RAC(self.mapNavigationModeView, selected) = [RACObserve(self.mapView, userTrackingMode) map:^NSNumber*(NSNumber *userTrackingMode) {
        return @([userTrackingMode isEqualToNumber:@(MKUserTrackingModeFollow)]);
    }];
    [[self.mapNavigationModeView rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *navigationModeButton) {
        @strongify(self)
        self.mapView.userTrackingMode = (navigationModeButton.selected) ? MKUserTrackingModeNone : MKUserTrackingModeFollow;
    }];
}

- (void)bindFilteredCategoriesViewModel {
    @weakify(self);
    [[self.filterCategoryView rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        self.filterViewModel = [[PMFilterCategoryViewModel alloc] initWithFilteredCategories:self.viewModel.filteredCategories];
        PMFilterCategoryViewController *filterCategoryViewController = [[PMFilterCategoryViewController alloc] initWithViewModel:self.filterViewModel];
        [self presentViewController:filterCategoryViewController animated:YES completion:nil];
        @weakify(self);
        [[self.filterViewModel.executeDoneButton.executionSignals switchToLatest] subscribeNext:^(NSSet *filteredCategories) {
            @strongify(self)
            self.viewModel.filteredCategories = filteredCategories;
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    }];
    [self.viewModel.filterPhotosSignal subscribeNext:^(NSArray *annotations) {
        @strongify(self)
        NSInteger toRemoveCount = self.mapView.annotations.count;
        NSMutableArray *toRemove = [NSMutableArray arrayWithCapacity:toRemoveCount];
        for (id annotation in self.mapView.annotations)
            if (annotation != self.mapView.userLocation)
                [toRemove addObject:annotation];
        [self.mapView removeAnnotations:toRemove];
        [self.mapView addAnnotations:annotations];
    }];
    [self.viewModel.executeLoadPhotos.errors subscribeNext:^(NSError *error) {
        @strongify(self)
        [PMUtils controller:self showAlertWithMessage:[error localizedDescription]];
    }];
}

- (void)bindPhotoLocationViewModel {
    @weakify(self);
    RACSignal *mapLongPressRecognisionSignal = [[self.mapLongPressGestureRecognizer.rac_gestureSignal filter:^BOOL(UILongPressGestureRecognizer *sender) {
        return sender.state == UIGestureRecognizerStateEnded;
    }] map:^id(UILongPressGestureRecognizer *sender) {
        @strongify(self)
        CGPoint touchPoint = [sender locationInView:self.mapView];
        CLLocationCoordinate2D coordinate = [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
        return [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    }];
    RACSignal *createPhotoButtonSignal = [[self.createPhotoView rac_signalForControlEvents:UIControlEventTouchUpInside] map:^id(UIButton *sender) {
        @strongify(self)
        CLLocationCoordinate2D coordinate = self.mapView.userLocation.coordinate;
        return [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    }];
    RAC(self.viewModel, photoLocation) = [RACSignal merge:@[mapLongPressRecognisionSignal, createPhotoButtonSignal]];
    [self.viewModel.attemptToCreatePhotoSignal subscribeNext:^(CLLocation *photoLocation) {
        @strongify(self)
        [self showPhotoCreationAlert];
    }];
}

- (void)bindNetworkAvailability {
    @weakify(self)
    [[[[AppDelegate sharedDelegate] reachableSignal] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(NSNumber *reachable) {
        @strongify(self)
        if ([reachable boolValue]) {
            [self.viewModel.executeLoadPhotos execute:nil];
        } else {
            [self performSelector:@selector(showSettingsAlert) withObject:nil afterDelay:0.5f];
        }
    }];
}

- (void)showSettingsAlert {
    [PMUtils controller:self showSettingsAlertWithMessage:NSLocalizedString(@"NoInternetConnection", @"")];
}

- (void)bindKeyboardAppearing {
    @weakify(self);
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillShowNotification object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(NSNotification *notification) {
        @strongify(self)
        NSValue* keyboardFrameBegin = [notification.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
        CGRect frame = self.createPhotoPopupView.frame;
        if ([[notification.userInfo valueForKey:UIKeyboardAnimationCurveUserInfoKey] intValue] != 0) {
            frame.origin.y -= [keyboardFrameBegin CGRectValue].size.height;
            self.createPhotoPopupView.frame = frame;
        }
    }];
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:UIKeyboardWillHideNotification object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(NSNotification *notification) {
        @strongify(self)
        NSValue* keyboardFrameEnd = [notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
        CGRect frame = self.createPhotoPopupView.frame;
        frame.origin.y += [keyboardFrameEnd CGRectValue].size.height;
        self.createPhotoPopupView.frame = frame;
    }];
}

- (void)setupSelectedCategory:(NSString*)newCategory {
    self.viewModel.selectedCategory = newCategory;
    [self.selectedCategoryView setTitle:[self.viewModel.selectedCategory uppercaseString] forState:UIControlStateNormal];
    [self.selectedCategoryView setImage:[PMPhoto imageIconForCategory:self.viewModel.selectedCategory] forState:UIControlStateNormal];
}

- (void)showPhotoCreationAlert {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    @weakify(self)
    UIAlertAction *takePictureAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"TakePicture", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self)
        [self showImagePickerWithType:UIImagePickerControllerSourceTypeCamera];
    }];
    UIAlertAction *chooseFromLibraryAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"ChooseFromLibrary", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self)
        [self showImagePickerWithType:UIImagePickerControllerSourceTypePhotoLibrary];
    }];
    [alertController addAction:takePictureAction];
    [alertController addAction:chooseFromLibraryAction];
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showImagePickerWithType:(UIImagePickerControllerSourceType)type {
    if ([UIImagePickerController isSourceTypeAvailable:type]) {
        self.imagePicker.sourceType = type;
        [self presentViewController:self.imagePicker animated:YES completion:nil];
    }
}

# pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.viewModel.categories count];
}

#pragma mark - UIPickerViewDelegate

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    return pickerView.bounds.size.width;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return kPickerViewCellHeight;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(nullable UIView *)view {
    PMCategoryView *categoryView = (PMCategoryView*) view;
    if (categoryView == nil) {
        categoryView = [[PMCategoryView alloc] initWithFrame:CGRectMake(0.f, 0.f, pickerView.bounds.size.width, kPickerViewCellHeight)];
    }
    NSString *category = [self.viewModel.categories objectAtIndex:row];
    categoryView.categoryTextView.text = category;
    categoryView.categoryIconView.image = [PMPhoto imageIconForCategory:category];
    
    return categoryView;
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

#pragma mark - MKMapViewDelegate

- (nullable MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    } else if ([annotation isKindOfClass:[PMPhotoAnnotation class]]) {
        NSString *reuseId = NSStringFromClass([PMPhotoPinAnnotationView class]);
        PMPhotoPinAnnotationView *pinAnnotationView = (PMPhotoPinAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
        if (pinAnnotationView == nil) {
            pinAnnotationView = [[PMPhotoPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
        } else {
            [pinAnnotationView updateViewWithAnnotation:annotation];
        }
        return pinAnnotationView;
    }
    
    return nil;
}

@end
