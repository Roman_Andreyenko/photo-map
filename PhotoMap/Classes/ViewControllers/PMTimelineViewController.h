//
//  PMTimelineViewController.h
//  PhotoMap
//
//  Created by mac-251 on 1/6/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PMTimelineViewModel;

@interface PMTimelineViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

- (instancetype)initWithViewModel:(PMTimelineViewModel *)viewModel;

@property (weak, nonatomic) IBOutlet UITableView *timelineTableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@end
