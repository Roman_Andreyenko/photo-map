//
//  PMMoreViewController.m
//  PhotoMap
//
//  Created by mac-251 on 2/19/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMMoreViewController.h"
#import "PMMoreViewModel.h"
#import "AppDelegate.h"
#import "PMUtils.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface PMMoreViewController ()

- (void)bindViewModel;

@property (weak, nonatomic) PMMoreViewModel *viewModel;

@end

@implementation PMMoreViewController

- (instancetype)initWithViewModel:(PMMoreViewModel *)viewModel {
    self = [super init];
    if (self ) {
        _viewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self bindViewModel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)bindViewModel {
    self.logOutView.rac_command = self.viewModel.executeLogOut;
    RAC(self.activityIndicatorView, hidden) = [self.viewModel.executeLogOut.executing not];
    @weakify(self)
    [[self.viewModel.executeLogOut.executionSignals switchToLatest] subscribeNext:^(id result) {
        [[AppDelegate sharedDelegate] setupRootViewControllerAnimated:YES];
    }];
    [self.viewModel.executeLogOut.errors subscribeNext:^(NSError *error) {
        @strongify(self)
        [PMUtils controller:self showAlertWithMessage:[error localizedDescription]];
    }];
}

@end
