//
//  PMMoreViewController.h
//  PhotoMap
//
//  Created by mac-251 on 2/19/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PMMoreViewModel;

@interface PMMoreViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *logOutView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

- (instancetype)initWithViewModel:(PMMoreViewModel *)viewModel;

@end
