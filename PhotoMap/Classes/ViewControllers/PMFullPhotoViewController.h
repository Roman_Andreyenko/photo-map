//
//  PMFullPhotoViewController.h
//  PhotoMap
//
//  Created by mac-251 on 3/30/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PMFullPhotoViewModel;

@interface PMFullPhotoViewController : UIViewController <UIScrollViewDelegate>

- (instancetype)initWithViewModel:(PMFullPhotoViewModel *)viewModel;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *backButtonView;
@property (weak, nonatomic) IBOutlet UIView *topBarView;
@property (weak, nonatomic) IBOutlet UIView *bottomBarView;
@property (weak, nonatomic) IBOutlet UILabel *photoDescriptionView;
@property (weak, nonatomic) IBOutlet UITextField *photoCreationDateView;
@property (weak, nonatomic) IBOutlet UIImageView *photoView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *hideTopBottomBarTapGestureRecognizer;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *zoomPhotoTapGestureRecognizer;

@end
