//
//  PMFilterCategoryViewController.m
//  PhotoMap
//
//  Created by mac-251 on 3/18/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMFilterCategoryViewController.h"
#import "PMFilterCategoryViewModel.h"
#import "UIColor+AppColors.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface PMFilterCategoryViewController ()

@property (weak, nonatomic) PMFilterCategoryViewModel *viewModel;

@end

@implementation PMFilterCategoryViewController

- (instancetype)initWithViewModel:(PMFilterCategoryViewModel *)viewModel {
    self = [super init];
    if (self ) {
        _viewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureCategoryView:self.natureCategoryView withBorderColor:[UIColor natureCategoryColor]];
    [self configureCategoryView:self.friendsCategoryView withBorderColor:[UIColor friendsCategoryColor]];
    [self configureCategoryView:self.defaultCategoryView withBorderColor:[UIColor defaultCategoryColor]];
    
    [self bindViewModel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureCategoryView:(UIButton*)categoryView withBorderColor:(UIColor*)borderColor {
    categoryView.layer.cornerRadius = categoryView.bounds.size.height / 2.f;
    categoryView.layer.borderWidth = 1.f;
    categoryView.layer.borderColor = [borderColor CGColor];
}

- (void)bindViewModel {
    self.navigationBar.topItem.rightBarButtonItem.rac_command = self.viewModel.executeDoneButton;
    [self bindFilterView:self.natureCategoryView filterColor:[UIColor natureCategoryColor]];
    [self bindFilterView:self.friendsCategoryView filterColor:[UIColor friendsCategoryColor]];
    [self bindFilterView:self.defaultCategoryView filterColor:[UIColor defaultCategoryColor]];
    self.natureCategoryView.selected = self.viewModel.natureFilterSelected;
    self.friendsCategoryView.selected = self.viewModel.friendsFilterSelected;
    self.defaultCategoryView.selected = self.viewModel.defaultFilterSelected;
    RAC(self.viewModel, natureFilterSelected) = RACObserve(self.natureCategoryView, selected);
    RAC(self.viewModel, friendsFilterSelected) = RACObserve(self.friendsCategoryView, selected);
    RAC(self.viewModel, defaultFilterSelected) = RACObserve(self.defaultCategoryView, selected);
}

- (void)bindFilterView:(UIButton*)filterView filterColor:(UIColor*)filterColor {
    [[filterView rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *button) {
        filterView.selected = !button.selected;
    }];
    RAC(filterView, backgroundColor) = [RACObserve(filterView, selected) map:^UIColor*(NSNumber *selected) {
        return [selected boolValue] ? filterColor : [UIColor whiteColor];
    }];
}

@end
