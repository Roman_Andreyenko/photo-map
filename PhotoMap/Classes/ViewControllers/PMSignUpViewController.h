//
//  PMSignUpViewController.h
//  PhotoMap
//
//  Created by mac-251 on 2/12/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PMSignUpViewModel;

@interface PMSignUpViewController : UIViewController <UITextFieldDelegate>

- (instancetype)initWithViewModel:(PMSignUpViewModel *)viewModel;

@property (weak, nonatomic) IBOutlet UITextField *usernameView;
@property (weak, nonatomic) IBOutlet UILabel *usernameValidationView;
@property (weak, nonatomic) IBOutlet UITextField *emailView;
@property (weak, nonatomic) IBOutlet UILabel *emailValidationView;
@property (weak, nonatomic) IBOutlet UITextField *passwordView;
@property (weak, nonatomic) IBOutlet UILabel *passwordValidationView;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordView;
@property (weak, nonatomic) IBOutlet UILabel *confirmPasswordValidationView;
@property (weak, nonatomic) IBOutlet UIButton *signUpView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@end
