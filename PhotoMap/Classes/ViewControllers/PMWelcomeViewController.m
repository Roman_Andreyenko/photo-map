//
//  PMWelcomeViewController.m
//  PhotoMap
//
//  Created by mac-251 on 2/12/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMWelcomeViewController.h"
#import "PMSignUpViewController.h"
#import "PMLogInViewController.h"
#import "PMSignUpViewModel.h"
#import "PMLogInViewModel.h"
#import "PMWelcomeViewModel.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface PMWelcomeViewController ()

- (void)bindViewModel;

@property (nonatomic, weak) PMWelcomeViewModel *viewModel;
@property (nonatomic, strong) PMSignUpViewModel *signUpViewModel;
@property (nonatomic, strong) PMLogInViewModel *logInViewModel;

@end

@implementation PMWelcomeViewController

- (instancetype)initWithViewModel:(PMWelcomeViewModel *)viewModel {
    self = [super init];
    if (self ) {
        _viewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self bindViewModel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)bindViewModel {
    self.redirectToLogInView.rac_command = self.viewModel.executeRedirectToLogIn;
    self.redirectToSignUpView.rac_command = self.viewModel.executeRedirectToSignUp;
    @weakify(self)
    [[self.viewModel.executeRedirectToSignUp.executionSignals switchToLatest] subscribeNext:^(PMSignUpViewModel* newModel) {
        @strongify(self)
        self.signUpViewModel = newModel;
        [self.navigationController pushViewController:[[PMSignUpViewController alloc] initWithViewModel:self.signUpViewModel] animated:YES];
    }];
    [[self.viewModel.executeRedirectToLogIn.executionSignals switchToLatest] subscribeNext:^(PMLogInViewModel *newModel) {
        @strongify(self)
        self.logInViewModel = newModel;
        [self.navigationController pushViewController:[[PMLogInViewController alloc] initWithViewModel:self.logInViewModel] animated:YES];
    }];
}

@end
