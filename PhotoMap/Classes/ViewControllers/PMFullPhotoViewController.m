//
//  PMFullPhotoViewController.m
//  PhotoMap
//
//  Created by mac-251 on 3/30/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMFullPhotoViewController.h"
#import "PMFullPhotoViewModel.h"
#import "PMUtils.h"
#import "PMPhoto.h"
#import "UIViewController+Transitions.h"
#import "AppDelegate.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface PMFullPhotoViewController ()

- (void)bindViewModel;

@property (weak, nonatomic) PMFullPhotoViewModel *viewModel;

@end

@implementation PMFullPhotoViewController

- (instancetype)initWithViewModel:(PMFullPhotoViewModel *)viewModel {
    self = [super init];
    if (self ) {
        _viewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupShadowForView:self.topBarView withOffsetSize:CGSizeMake(0.f, 4.f)];
    [self setupShadowForView:self.bottomBarView withOffsetSize:CGSizeMake(0.f, -4.f)];
    [self.hideTopBottomBarTapGestureRecognizer requireGestureRecognizerToFail:self.zoomPhotoTapGestureRecognizer];
    
    [self bindViewModel];
    
    self.scrollView.delegate = self;
}

- (void)setupShadowForView:(UIView*)view withOffsetSize:(CGSize)offsetSize {
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = offsetSize;
    view.layer.shadowRadius = 2.f;
    view.layer.shadowOpacity = 0.9f;
    view.layer.shadowColor = [[UIColor blackColor] CGColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)bindViewModel {
    
    @weakify(self)
    [[self.viewModel.executePhotoLoading.executionSignals switchToLatest] subscribeNext:^(UIImage *photoImage) {
        @strongify(self)
        self.photoView.image = photoImage;
    }];
    [self.viewModel.executePhotoLoading.errors subscribeNext:^(NSError *error) {
        @strongify(self)
        [PMUtils controller:self showAlertWithMessage:[error localizedDescription]];
    }];
    RAC(self.activityIndicatorView, hidden) = [self.viewModel.executePhotoLoading.executing not];
    [[self.backButtonView rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        [self dismissHorizontallyViewControllerAnimated:NO completion:nil];
    }];
    [self.hideTopBottomBarTapGestureRecognizer.rac_gestureSignal subscribeNext:^(id x) {
        @strongify(self)
        [self hideBars];
    }];
    [self.zoomPhotoTapGestureRecognizer.rac_gestureSignal subscribeNext:^(id x) {
        @strongify(self)
        [self.scrollView setZoomScale:MIN(self.scrollView.maximumZoomScale, self.scrollView.zoomScale * 2.f) animated:YES];
    }];
    [[[[AppDelegate sharedDelegate] reachableSignal] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(NSNumber *reachable) {
        @strongify(self)
        if ([reachable boolValue]) {
            [self.viewModel.executePhotoLoading execute:[self rac_willDeallocSignal]];
        } else {
            [self performSelector:@selector(showSettingsAlert) withObject:nil afterDelay:0.5f];
        }
    }];
    self.photoDescriptionView.text = self.viewModel.photo.descriptionText;
    self.photoCreationDateView.text = [self.viewModel formatPhotoCreatedDate];
}

- (void)showSettingsAlert {
    [PMUtils controller:self showSettingsAlertWithMessage:NSLocalizedString(@"NoInternetConnection", @"")];
}

- (void)hideBars {
    BOOL sourceTopBarHidden = self.topBarView.hidden;
    BOOL sourceBottomBarHidden = self.bottomBarView.hidden;
    CGRect sourceTopBarFrame = self.topBarView.frame;
    CGRect destinationTopBarFrame = self.topBarView.frame;
    CGRect sourceBottomBarFrame = self.bottomBarView.frame;
    CGRect destinationBottomBarFrame = self.bottomBarView.frame;
    if (sourceTopBarHidden) {
        sourceTopBarFrame.origin.y -= sourceTopBarFrame.size.height;
    } else {
        destinationTopBarFrame.origin.y -= destinationTopBarFrame.size.height;
    }
    if (sourceBottomBarHidden) {
        sourceBottomBarFrame.origin.y += sourceBottomBarFrame.size.height;
    } else {
        destinationBottomBarFrame.origin.y += destinationBottomBarFrame.size.height;
    }
    self.topBarView.hidden = NO;
    self.topBarView.frame = sourceTopBarFrame;
    self.bottomBarView.hidden = NO;
    self.bottomBarView.frame = sourceBottomBarFrame;
    self.hideTopBottomBarTapGestureRecognizer.enabled = NO;
    @weakify(self)
    [UIView animateWithDuration:0.3f animations:^{
        @strongify(self)
        self.topBarView.frame = destinationTopBarFrame;
        self.bottomBarView.frame = destinationBottomBarFrame;
    } completion:^(BOOL finished) {
        @strongify(self)
        self.hideTopBottomBarTapGestureRecognizer.enabled = YES;
        self.topBarView.hidden = !sourceTopBarHidden;
        self.topBarView.frame = (sourceTopBarHidden) ? destinationTopBarFrame : sourceTopBarFrame;
        self.bottomBarView.hidden = !sourceBottomBarHidden;
        self.bottomBarView.frame = (sourceBottomBarHidden) ? destinationBottomBarFrame : sourceBottomBarFrame;
    }];
}

#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.photoView;
}

@end
