//
//  PMLogInViewController.h
//  PhotoMap
//
//  Created by mac-251 on 2/12/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PMLogInViewModel;

@interface PMLogInViewController : UIViewController <UITextFieldDelegate>

- (instancetype)initWithViewModel:(PMLogInViewModel *)viewModel;

@property (weak, nonatomic) IBOutlet UITextField *usernameView;
@property (weak, nonatomic) IBOutlet UILabel *usernameValidationView;
@property (weak, nonatomic) IBOutlet UITextField *passwordView;
@property (weak, nonatomic) IBOutlet UILabel *passwordValidationView;
@property (weak, nonatomic) IBOutlet UIButton *logInView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@end
