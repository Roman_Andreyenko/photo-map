//
//  PMTimelineViewController.m
//  PhotoMap
//
//  Created by mac-251 on 1/6/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMTimelineViewController.h"
#import "PMTimelineViewModel.h"
#import "PMPhoto.h"
#import "PMPhotoIndex.h"
#import "PMPhotoSectionWrapper.h"
#import "PMPhotoTableViewCell.h"
#import "PMFilterCategoryViewModel.h"
#import "PMFilterCategoryViewController.h"
#import "PMPhotoTableSectionHeaderView.h"
#import "PMFullPhotoViewController.h"
#import "PMFullPhotoViewModel.h"
#import "UIViewController+Transitions.h"
#import "PMUtils.h"
#import "AppDelegate.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface PMTimelineViewController ()

- (void)bindViewModel;

@property (strong, nonatomic) PMFilterCategoryViewModel *filterViewModel;
@property (strong, nonatomic) PMFullPhotoViewModel *fullPhotoViewModel;
@property (weak, nonatomic) PMTimelineViewModel *viewModel;
@property (weak, nonatomic) UISearchBar *searchBar;

@end

@interface PMTimelineViewController (ForwardedMethods)

- (void)categoryFilterClick:(UIBarButtonItem*)item;

@end

@implementation PMTimelineViewController

- (instancetype)initWithViewModel:(PMTimelineViewModel *)viewModel {
    self = [super init];
    if (self ) {
        _viewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:self.navigationItem.titleView.frame];
    self.searchBar = searchBar;
    self.searchBar.enablesReturnKeyAutomatically = NO;
    self.searchBar.delegate = self;
    self.navigationItem.titleView = searchBar;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Category", @"") style:UIBarButtonItemStylePlain target:self action:@selector(categoryFilterClick:)];
    
    self.timelineTableView.backgroundView = nil;
    self.timelineTableView.backgroundColor = [UIColor whiteColor];
    [self.timelineTableView registerNib:[UINib nibWithNibName:NSStringFromClass([PMPhotoTableViewCell class]) bundle:[NSBundle mainBundle]] forCellReuseIdentifier:NSStringFromClass([PMPhotoTableViewCell class])];
    [self.timelineTableView registerNib:[UINib nibWithNibName:NSStringFromClass([PMPhotoTableSectionHeaderView class]) bundle:[NSBundle mainBundle]] forHeaderFooterViewReuseIdentifier:NSStringFromClass([PMPhotoTableSectionHeaderView class])];
    
    [self bindViewModel];
    
    self.timelineTableView.dataSource = self;
    self.timelineTableView.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)bindViewModel {
    RAC(self.activityIndicatorView, hidden) = [self.viewModel.executeSearchPhotos.executing not];
    RAC(self.view, userInteractionEnabled) = [self.viewModel.executeSearchPhotos.executing not];
    RAC(self.timelineTableView, hidden) = self.viewModel.executeSearchPhotos.executing;
    [[self rac_signalForSelector:@selector(tableView:willDisplayCell:forRowAtIndexPath:)] subscribeNext:^(RACTuple *tuple) {
        UITableViewCell* cell = tuple.second;
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
            [cell setPreservesSuperviewLayoutMargins:NO];
        }
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
    }];
    [[self rac_signalForSelector:@selector(tableView:didSelectRowAtIndexPath:)] subscribeNext:^(RACTuple *tuple) {
        RACTupleUnpack(UITableView *tableView, NSIndexPath *indexPath) = tuple;
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        self.fullPhotoViewModel = [[PMFullPhotoViewModel alloc] initWithPhoto:[self.viewModel photoForIndexPath:indexPath]];
        PMFullPhotoViewController *fullPhotoViewController = [[PMFullPhotoViewController alloc] initWithViewModel:self.fullPhotoViewModel];
        [self presentHorizontallyViewController:fullPhotoViewController animated:NO completion:nil];
    }];
    RAC(self.viewModel, searchText) = [[self rac_signalForSelector:@selector(searchBar:textDidChange:)] map:^NSString*(RACTuple *tuple) {
        return tuple.second;
    }];
    @weakify(self)
    [self.viewModel.executeSearchPhotos.errors subscribeNext:^(NSError *error) {
        @strongify(self)
        [PMUtils controller:self showAlertWithMessage:[error localizedDescription]];
    }];
    [[self rac_signalForSelector:@selector(searchBarSearchButtonClicked:)] subscribeNext:^(id x) {
        @strongify(self)
        [self.searchBar resignFirstResponder];
        [self searchPhotosIfNetworkReachable:[[AppDelegate sharedDelegate].reachibility isReachable]];
    }];
    [RACObserve(self.viewModel, photoSections) subscribeNext:^(NSArray *photoSection) {
        @strongify(self)
        [self.timelineTableView reloadData];
    }];
    [[self rac_signalForSelector:@selector(categoryFilterClick:)] subscribeNext:^(id x) {
        @strongify(self)
        self.filterViewModel = [[PMFilterCategoryViewModel alloc] initWithFilteredCategories:self.viewModel.filteredCategories];
        PMFilterCategoryViewController *filterCategoryViewController = [[PMFilterCategoryViewController alloc] initWithViewModel:self.filterViewModel];
        [self presentViewController:filterCategoryViewController animated:YES completion:nil];
        @weakify(self);
        [[self.filterViewModel.executeDoneButton.executionSignals switchToLatest] subscribeNext:^(NSSet *filteredCategories) {
            @strongify(self)
            self.viewModel.filteredCategories = filteredCategories;
            [self dismissViewControllerAnimated:YES completion:nil];
            [self searchPhotosIfNetworkReachable:[[AppDelegate sharedDelegate].reachibility isReachable]];            
        }];
    }];
    [[[[AppDelegate sharedDelegate] reachableSignal] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(NSNumber *reachable) {
        @strongify(self)
        [self searchPhotosIfNetworkReachable:[reachable boolValue]];
    }];
}

- (void)searchPhotosIfNetworkReachable:(BOOL)reachable {
    if (reachable) {
        [self.viewModel.executeSearchPhotos execute:nil];
    } else {
        [PMUtils controller:self showSettingsAlertWithMessage:NSLocalizedString(@"NoInternetConnection", @"")];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.viewModel.photoSections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    PMPhotoSectionWrapper *wrapper = [self.viewModel.photoSections objectAtIndex:section];
    return wrapper.size;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PMPhotoTableViewCell *cell = (PMPhotoTableViewCell*)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PMPhotoTableViewCell class])];
    PMPhoto *photo = [self.viewModel photoForIndexPath:indexPath];
    [cell updateCellWithPhoto:photo];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    PMPhotoTableSectionHeaderView *view = (PMPhotoTableSectionHeaderView*)[tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([PMPhotoTableSectionHeaderView class])];
    return view.bounds.size.height;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    PMPhotoTableSectionHeaderView *view = (PMPhotoTableSectionHeaderView*)[tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([PMPhotoTableSectionHeaderView class])];
    PMPhoto *photo = [self.viewModel photoForIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
    [view updateViewWithPhoto:photo];
    return view;
}

@end
