//
//  PMMapViewController.h
//  PhotoMap
//
//  Created by mac-251 on 1/6/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@class PMMapViewModel;

@interface PMMapViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate, MKMapViewDelegate, UITextViewDelegate>

- (instancetype)initWithViewModel:(PMMapViewModel *)viewModel;

@property (weak, nonatomic) IBOutlet UIButton *filterCategoryView;
@property (weak, nonatomic) IBOutlet UIButton *createPhotoView;
@property (weak, nonatomic) IBOutlet UIButton *mapNavigationModeView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (weak, nonatomic) IBOutlet UILongPressGestureRecognizer *mapLongPressGestureRecognizer;
@property (weak, nonatomic) IBOutlet UIView *createPhotoPopupOverlayView;
@property (weak, nonatomic) IBOutlet UIView *createPhotoPopupView;
@property (weak, nonatomic) IBOutlet UIImageView *pickedPhotoView;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *pickedPhotoViewTapGestureRecognizer;
@property (weak, nonatomic) IBOutlet UILabel *pickedPhotoCreatedDateView;
@property (weak, nonatomic) IBOutlet UIButton *selectedCategoryView;
@property (weak, nonatomic) IBOutlet UITextView *createPhotoDescriptionTextView;
@property (weak, nonatomic) IBOutlet UIPickerView *categoryPickerView;
@property (weak, nonatomic) IBOutlet UIView *categoryPickerOverlay;
@property (weak, nonatomic) IBOutlet UIButton *categoryPickerDoneView;
@property (weak, nonatomic) IBOutlet UIButton *createPhotoDoneView;
@property (weak, nonatomic) IBOutlet UIButton *createPhotoCancelView;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *selectedPinTapGestureRecognizer;

@end
