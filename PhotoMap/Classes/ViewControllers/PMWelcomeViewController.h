//
//  PMWelcomeViewController.h
//  PhotoMap
//
//  Created by mac-251 on 2/12/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PMWelcomeViewModel;

@interface PMWelcomeViewController : UIViewController

- (instancetype)initWithViewModel:(PMWelcomeViewModel *)viewModel;

@property (weak, nonatomic) IBOutlet UIButton *redirectToSignUpView;
@property (weak, nonatomic) IBOutlet UIButton *redirectToLogInView;

@end
