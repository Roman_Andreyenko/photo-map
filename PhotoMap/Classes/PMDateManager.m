//
//  PMDateManager.m
//  PhotoMap
//
//  Created by mac-251 on 3/30/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMDateManager.h"

@implementation PMDateManager

+ (instancetype)defaultDateManager
{
    static PMDateManager *dateManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        dateManager = [PMDateManager new];
    });
    
    return dateManager;
}

+ (NSString*)daySuffixForDate:(NSDate*)date {
    NSInteger dayOfMonth = [[NSCalendar currentCalendar] component:NSCalendarUnitDay fromDate:date];
    switch (dayOfMonth) {
        case 1:
        case 21:
        case 31: return @"st";
        case 2:
        case 22: return @"nd";
        case 3:
        case 23: return @"rd";
        default: return @"th";
    }
}

- (instancetype)init {
    if (self = [super init]) {
        [self initializeFullMonthYearDateFormatter];
        [self initializeShortDateFormatter];
        [self initializeCreatedPhotoDateFormatter];
        [self initializeCreatedFullPhotoDateFormatter];
    }
    return self;
}

- (void)initializeFullMonthYearDateFormatter {
    self.fullMonthYearDateFormatter = [NSDateFormatter new];
    self.fullMonthYearDateFormatter.dateFormat = @"MMMM yyyy";
}

- (void)initializeShortDateFormatter {
    self.shortDateFormatter = [NSDateFormatter new];
    self.shortDateFormatter.dateFormat = @"MM-dd-yyyy";
}

- (void)initializeCreatedPhotoDateFormatter {
    self.createdPhotoDateFormatter = [NSDateFormatter new];
    self.createdPhotoDateFormatter.dateFormat = @"MMMM d'%@', yyyy - h:mm a";
    self.createdPhotoDateFormatter.AMSymbol = @"am";
    self.createdPhotoDateFormatter.PMSymbol = @"pm";
}

- (void)initializeCreatedFullPhotoDateFormatter {
    self.createdFullPhotoDateFormatter = [NSDateFormatter new];
    self.createdFullPhotoDateFormatter.dateFormat = @"MMMM d'%@', yyyy 'at' h:mm a";
    self.createdFullPhotoDateFormatter.AMSymbol = @"am";
    self.createdFullPhotoDateFormatter.PMSymbol = @"pm";
}

@end
