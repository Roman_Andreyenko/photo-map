//
//  UIViewController+TransitionsViewController.m
//  PhotoMap
//
//  Created by mac-251 on 3/30/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "UIViewController+Transitions.h"

@implementation UIViewController (Transitions)

- (void)presentHorizontallyViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    
    [self presentViewController:viewControllerToPresent animated:flag completion:completion];
}

- (void)dismissHorizontallyViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self dismissViewControllerAnimated:flag completion:completion];
}

@end
