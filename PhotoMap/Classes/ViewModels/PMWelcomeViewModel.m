//
//  PMWelcomeViewModel.m
//  PhotoMap
//
//  Created by mac-251 on 2/24/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMWelcomeViewModel.h"
#import "PMLogInViewModel.h"
#import "PMSignUpViewModel.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@implementation PMWelcomeViewModel

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    @weakify(self)
    self.executeRedirectToLogIn = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self)
        return [self executeRedirectToLogInSignal];
    }];
    
    self.executeRedirectToSignUp = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self)        
        return [self executeRedirectToSignUpSignal];
    }];
}

- (RACSignal*)executeRedirectToLogInSignal {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [subscriber sendNext:[PMLogInViewModel new]];
        [subscriber sendCompleted];
        
        return nil;
    }];
}

- (RACSignal*)executeRedirectToSignUpSignal {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [subscriber sendNext:[PMSignUpViewModel new]];
        [subscriber sendCompleted];
        
        return nil;
    }];
}

@end
