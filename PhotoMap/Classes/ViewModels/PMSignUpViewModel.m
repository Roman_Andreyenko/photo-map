//
//  SignUpViewModel.m
//  PhotoMap
//
//  Created by mac-251 on 2/12/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMSignUpViewModel.h"
#import "AppDelegate.h"
#import <Parse/Parse.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface PMSignUpViewModel ()

@property (strong, nonatomic) NSRegularExpression *usernameRegex;
@property (strong, nonatomic) NSRegularExpression *emailRegex;
@property (strong, nonatomic) NSRegularExpression *passwordRegex;

@end

@implementation PMSignUpViewModel

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    self.potentialUser = [PFUser user];
    
    self.usernameRegex = [NSRegularExpression regularExpressionWithPattern:@"^\\w{3,8}$" options:0 error:nil];
    self.emailRegex = [NSRegularExpression regularExpressionWithPattern:@"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" options:0 error:nil];
    self.passwordRegex = [NSRegularExpression regularExpressionWithPattern:@"^.{5,20}$" options:0 error:nil];
    
    @weakify(self)
    
    self.validUsernameSignal =
    [[RACObserve(self.potentialUser, username) map:^id(NSString* value) {
        @strongify(self)
        if (value) {
            NSUInteger numberOfMatches = [self.usernameRegex numberOfMatchesInString:value options:0 range:NSMakeRange(0, [value length])];
            return numberOfMatches == 0 ? NSLocalizedString(@"ValidUsername", @"") : nil;
        } else {
            return NSLocalizedString(@"ValidUsername", @"");
        }
    }] distinctUntilChanged];
    
    self.validEmailSignal =
    [[RACObserve(self.potentialUser, email) map:^id(NSString* value) {
        @strongify(self)
        if (value) {
            NSUInteger numberOfMatches = [self.emailRegex numberOfMatchesInString:value options:0 range:NSMakeRange(0, [value length])];
            return numberOfMatches == 0 ? NSLocalizedString(@"ValidEmail", @"") : nil;
        } else {
            return NSLocalizedString(@"ValidEmail", @"");
        }
    }] distinctUntilChanged];
    
    self.validPasswordSignal =
    [[RACObserve(self.potentialUser, password) map:^id(NSString* value) {
        @strongify(self)
        if (value) {
            NSUInteger numberOfMatches = [self.passwordRegex numberOfMatchesInString:value options:0 range:NSMakeRange(0, [value length])];
            return numberOfMatches == 0 ? NSLocalizedString(@"ValidPassword", @"") : nil;
        } else {
            return NSLocalizedString(@"ValidPassword", @"");
        }
    }] distinctUntilChanged];
    
    self.validConfirmPasswordSignal =
    [[RACObserve(self, confirmPasswordText) map:^id(NSString* value) {
        @strongify(self)
        if (value) {
            return [self.potentialUser.password isEqualToString:value] ? nil : NSLocalizedString(@"ValidConfirmPassword", @"");
        } else {
            return NSLocalizedString(@"ValidConfirmPassword", @"");
        }
    }] distinctUntilChanged];
    
    RACSignal *signUpActiveSignal = [RACSignal combineLatest:@[self.validUsernameSignal, self.validEmailSignal, self.validPasswordSignal, self.validConfirmPasswordSignal, [[[AppDelegate sharedDelegate] reachableSignal] takeUntil:[self rac_willDeallocSignal]]]
                                                      reduce:^id(NSString *usernameError, NSString *emailError, NSString *passwordError, NSString *confirmPasswordError, NSNumber* reachable) {
        return @(usernameError == nil && emailError == nil && passwordError == nil && confirmPasswordError == nil && [reachable boolValue]);
    }];
    
    self.executeSignUp = [[RACCommand alloc] initWithEnabled:signUpActiveSignal signalBlock:^RACSignal *(id input) {
        @strongify(self)
        return [self executeSignUpSignal];
    }];
}

- (RACSignal*)executeSignUpSignal {
    @weakify(self)
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        [self.potentialUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
            if (succeeded) {
                [subscriber sendNext:@(succeeded)];
                [subscriber sendCompleted];
            } else {
                [subscriber sendError:error];
            }
        }];
        
        return nil;
    }];
}

@end
