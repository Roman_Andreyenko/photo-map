//
//  PMMoreViewModel.m
//  PhotoMap
//
//  Created by mac-251 on 2/19/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMMoreViewModel.h"
#import <Parse/Parse.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "AppDelegate.h"

@implementation PMMoreViewModel

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    @weakify(self)
    
    self.executeLogOut = [[RACCommand alloc] initWithEnabled:[[[AppDelegate sharedDelegate] reachableSignal] takeUntil:[self rac_willDeallocSignal]] signalBlock:^RACSignal *(id input) {
        @strongify(self)
        return [self executeLogOutSignal];
    }];
}

- (RACSignal*)executeLogOutSignal {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [PFUser logOutInBackgroundWithBlock:^(NSError * _Nullable error) {
            if (error) {
                [subscriber sendError:error];
            } else {
                [subscriber sendNext:nil];
                [subscriber sendCompleted];
            }
        }];
        
        return nil;
    }];
}

@end
