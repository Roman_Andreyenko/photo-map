//
//  PMFilterCategoryViewModel.h
//  PhotoMap
//
//  Created by mac-251 on 3/21/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACCommand;

@interface PMFilterCategoryViewModel : NSObject

@property (assign, nonatomic) BOOL natureFilterSelected;
@property (assign, nonatomic) BOOL friendsFilterSelected;
@property (assign, nonatomic) BOOL defaultFilterSelected;

@property (strong, nonatomic) RACCommand *executeDoneButton;

- (instancetype)initWithFilteredCategories:(NSSet*)filteredCategories;

@end
