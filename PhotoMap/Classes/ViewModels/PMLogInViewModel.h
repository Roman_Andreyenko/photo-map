//
//  PMLogInViewModel.h
//  PhotoMap
//
//  Created by mac-251 on 2/19/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACCommand;
@class RACSignal;

@interface PMLogInViewModel : NSObject

@property (strong, nonatomic) NSString *usernameText;
@property (strong, nonatomic) NSString *passwordText;

@property (strong, nonatomic) RACCommand *executeLogIn;
@property (strong, nonatomic) RACSignal *validUsernameSignal;
@property (strong, nonatomic) RACSignal *validPasswordSignal;

@end
