//
//  PMMapViewModel.h
//  PhotoMap
//
//  Created by mac-251 on 2/24/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class RACCommand;
@class RACSignal;
@class PMPhoto;
@class UIImage;

@interface PMMapViewModel : NSObject

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *photoLocation;
@property (strong, nonatomic) NSDate *photoCreatedDate;
@property (strong, nonatomic) NSArray *categories;
@property (strong, nonatomic) NSString *selectedCategory;
@property (strong, nonatomic) NSString *photoDescriptionText;
@property (strong, nonatomic) NSMutableArray *photos;
@property (strong, nonatomic) NSSet *filteredCategories;

@property (strong, nonatomic) RACCommand *executeCreatePhoto;
@property (strong, nonatomic) RACCommand *executeLoadPhotos;

@property (strong, nonatomic) RACSignal *attemptToCreatePhotoSignal;
@property (strong, nonatomic) RACSignal *filterPhotosSignal;

- (NSString*)formatPhotoCreatedDate;
- (RACSignal*)loadPhotoThumbnailDataSignalWithPhoto:(PMPhoto*)photo takeUntil:(RACSignal*)takeUntilSignal;
- (void)requestUserLocationAuthorization;
- (void)setPhotoDataWithImage:(UIImage*)originalImage;
- (void)clearPhotoData;
- (PMPhoto*)photoWithPhotoData;

@end
