//
//  PMTimelineViewModel.m
//  PhotoMap
//
//  Created by mac-251 on 3/22/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMTimelineViewModel.h"
#import "PMPhoto.h"
#import "PMHashtag.h"
#import "PMPhotoIndex.h"
#import "PMPhotoSectionWrapper.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface PMTimelineViewModel ()

@end

@implementation PMTimelineViewModel

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    self.searchText = @"";
    self.filteredCategories = [NSSet setWithArray:@[kDefaultCategory, kNatureCategory, kFriendsCategory]];
    self.photos = [NSArray array];
    self.photoSections = [NSArray array];
    
    @weakify(self)
    self.executeSearchPhotos = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self)
        return [self searchPhotosSignal];
    }];
    
    [RACObserve(self, photos) subscribeNext:^(NSArray *photos) {
        @strongify(self)
        [self calculatePhotoSectionsWithPhotos:photos];
    }];
}

- (PMPhoto*)photoForIndexPath:(NSIndexPath*)indexPath {
    PMPhotoSectionWrapper *sectionWrapper = [self.photoSections objectAtIndex:indexPath.section];
    return [self.photos objectAtIndex:sectionWrapper.startIndex + indexPath.row];
}

- (RACSignal*)searchPhotosSignal {
    NSArray* hashtags = [PMHashtag parseTagsInText:self.searchText];
    return (hashtags.count > 0) ? [self searchPhotosSignalWithHashtags:hashtags] : [self searchPhotosWithoutPhotoIndexesSignal];
}

- (RACSignal*)searchPhotosSignalWithHashtags:(NSArray*)hashtags {
    @weakify(self)
    return [[self searchPhotoIndexesSignalWithHashtags:[PMHashtag parseTagsInText:self.searchText]] flattenMap:^RACStream *(NSArray* photoIndexes) {
        @strongify(self)
        return [self searchPhotosSignalWithPhotoIndexes:photoIndexes];
    }];
}

- (RACSignal*)searchPhotosWithoutPhotoIndexesSignal {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        PFQuery* query = [PMPhoto query];
        NSString *ownerKey = NSStringFromSelector(@selector(owner));
        [query whereKey:ownerKey equalTo:[PFUser currentUser]];
        NSString *categoryKey = NSStringFromSelector(@selector(category));
        [query whereKey:categoryKey containedIn:[self.filteredCategories allObjects]];
        NSString *createdTimeKey = NSStringFromSelector(@selector(createdTime));
        [query orderByDescending:createdTimeKey];
        [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
            if (error) {
                [subscriber sendError:error];
            } else {
                self.photos = objects;
                [subscriber sendNext:objects];
                [subscriber sendCompleted];
            }
        }];
        
        return nil;
    }];
}

- (RACSignal*)searchPhotosSignalWithPhotoIndexes:(NSArray*)photoIndexes {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSMutableArray* photoIndexesIds = [NSMutableArray arrayWithCapacity:photoIndexes.count];
        for (PMPhotoIndex *photoIndex in photoIndexes) {
            [photoIndexesIds addObject:photoIndex.photo.objectId];
        }
        PFQuery* query = [PMPhoto query];
        NSString *objectIdKey = NSStringFromSelector(@selector(objectId));
        [query whereKey:objectIdKey containedIn:photoIndexesIds];
        NSString *ownerKey = NSStringFromSelector(@selector(owner));
        [query whereKey:ownerKey equalTo:[PFUser currentUser]];
        NSString *categoryKey = NSStringFromSelector(@selector(category));
        [query whereKey:categoryKey containedIn:[self.filteredCategories allObjects]];
        NSString *createdTimeKey = NSStringFromSelector(@selector(createdTime));
        [query orderByDescending:createdTimeKey];
        [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
            if (error) {
                [subscriber sendError:error];
            } else {
                self.photos = objects;
                [subscriber sendNext:objects];
                [subscriber sendCompleted];
            }
        }];
        
        return nil;
    }];
}

- (RACSignal*)searchPhotoIndexesSignalWithHashtags:(NSArray*)hashtags {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        PFQuery *innerHashtagQuery = [PMHashtag query];
        NSString *hashtagKey = NSStringFromSelector(@selector(hashtag));
        [innerHashtagQuery whereKey:hashtagKey containedIn:hashtags];
        PFQuery *query = [PMPhotoIndex query];
        [query whereKey:hashtagKey matchesQuery:innerHashtagQuery];
        [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
            if (error) {
                [subscriber sendError:error];
            } else {
                [subscriber sendNext:objects];
                [subscriber sendCompleted];
            }
        }];
        
        return nil;
    }];
}

- (void)calculatePhotoSectionsWithPhotos:(NSArray*)photos {
    NSInteger currentMonth = 0;
    PMPhotoSectionWrapper *currentPhotoSectionWrapper = [[PMPhotoSectionWrapper alloc] initWithStartIndex:0 size:0];
    NSMutableArray *sections = [NSMutableArray array];
    for (NSInteger i = 0; i < photos.count; i++) {
        PMPhoto *photo = [photos objectAtIndex:i];
        NSDateComponents* components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth fromDate:photo.createdTime];
        if (currentMonth != components.month) {
            currentMonth = components.month;
            currentPhotoSectionWrapper.size = i - currentPhotoSectionWrapper.startIndex;
            currentPhotoSectionWrapper = [[PMPhotoSectionWrapper alloc] initWithStartIndex:i size:0];
            [sections addObject:currentPhotoSectionWrapper];
        }
    }
    currentPhotoSectionWrapper.size = photos.count - currentPhotoSectionWrapper.startIndex;
    self.photoSections = sections;
}

@end
