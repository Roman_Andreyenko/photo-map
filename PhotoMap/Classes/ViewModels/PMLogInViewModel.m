//
//  PMLogInViewModel.m
//  PhotoMap
//
//  Created by mac-251 on 2/19/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMLogInViewModel.h"
#import <Parse/Parse.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "AppDelegate.h"

@interface PMLogInViewModel ()

@property (strong, nonatomic) NSRegularExpression *usernameRegex;
@property (strong, nonatomic) NSRegularExpression *passwordRegex;

@end

@implementation PMLogInViewModel

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    self.usernameText = @"";
    self.passwordText = @"";
    
    self.usernameRegex = [NSRegularExpression regularExpressionWithPattern:@"^\\w{3,8}$" options:0 error:nil];
    self.passwordRegex = [NSRegularExpression regularExpressionWithPattern:@"^.{5,20}$" options:0 error:nil];
    
    @weakify(self)
    
    self.validUsernameSignal =
    [[RACObserve(self, usernameText) map:^id(NSString* value) {
        @strongify(self)
        if (value) {
            NSUInteger numberOfMatches = [self.usernameRegex numberOfMatchesInString:value options:0 range:NSMakeRange(0, [value length])];
            return numberOfMatches == 0 ? NSLocalizedString(@"ValidUsername", @"") : nil;
        } else {
            return NSLocalizedString(@"ValidUsername", @"");
        }
    }] distinctUntilChanged];
    
    self.validPasswordSignal =
    [[RACObserve(self, passwordText) map:^id(NSString* value) {
        @strongify(self)
        if (value) {
            NSUInteger numberOfMatches = [self.passwordRegex numberOfMatchesInString:value options:0 range:NSMakeRange(0, [value length])];
            return numberOfMatches == 0 ? NSLocalizedString(@"ValidPassword", @"") : nil;
        } else {
            return NSLocalizedString(@"ValidPassword", @"");
        }
    }] distinctUntilChanged];
    
    RACSignal *logInActiveSignal = [RACSignal combineLatest:@[self.validUsernameSignal, self.validPasswordSignal, [[[AppDelegate sharedDelegate] reachableSignal] takeUntil:[self rac_willDeallocSignal]]]
                                                      reduce:^id(NSString *usernameError, NSString *passwordError, NSNumber *reachable) {
                                                          return @(usernameError == nil && passwordError == nil && [reachable boolValue]);
                                                      }];
    
    self.executeLogIn = [[RACCommand alloc] initWithEnabled:logInActiveSignal signalBlock:^RACSignal *(id input) {
        @strongify(self)
        return [self executeLogInSignal];
    }];
}

- (RACSignal*)executeLogInSignal {
    @weakify(self)
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        [PFUser logInWithUsernameInBackground:self.usernameText password:self.passwordText block:^(PFUser * _Nullable user, NSError * _Nullable error) {
            if (error) {
                [subscriber sendError:error];
            } else {
                [subscriber sendNext:user];
                [subscriber sendCompleted];
            }
        }];
        
        return nil;
    }];
}

@end
