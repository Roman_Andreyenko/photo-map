//
//  PMFullPhotoViewModel.m
//  PhotoMap
//
//  Created by mac-251 on 3/30/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMFullPhotoViewModel.h"
#import "PMPhoto.h"
#import "PMDateManager.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@implementation PMFullPhotoViewModel

- (instancetype)initWithPhoto:(PMPhoto *)photo {
    if (self = [super init]) {
        [self initializeWithPhoto:photo];
    }
    return self;
}

- (void)initializeWithPhoto:(PMPhoto*)photo {
    
    self.photo = photo;
    
    @weakify(self)
    self.executePhotoLoading = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(RACSignal* takeUntilSignal) {
        @strongify(self)
        return [self.photo loadPhotoDataSignalTakeUntil:takeUntilSignal];
    }];
}

- (NSString*)formatPhotoCreatedDate {
    return [NSString stringWithFormat:[[PMDateManager defaultDateManager].createdFullPhotoDateFormatter stringFromDate:self.photo.createdTime], [PMDateManager daySuffixForDate:self.photo.createdTime]];
}

@end
