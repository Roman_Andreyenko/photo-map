//
//  PMWelcomeViewModel.h
//  PhotoMap
//
//  Created by mac-251 on 2/24/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACCommand;

@interface PMWelcomeViewModel : NSObject

@property (strong, nonatomic) RACCommand *executeRedirectToLogIn;
@property (strong, nonatomic) RACCommand *executeRedirectToSignUp;

@end
