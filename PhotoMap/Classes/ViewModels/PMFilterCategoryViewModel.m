//
//  PMFilterCategoryViewModel.m
//  PhotoMap
//
//  Created by mac-251 on 3/21/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMFilterCategoryViewModel.h"
#import "PMPhoto.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@implementation PMFilterCategoryViewModel

- (instancetype)initWithFilteredCategories:(NSSet*)filteredCategories {
    if (self = [super init]) {
        [self initializeWithFilteredCategories:filteredCategories];
    }
    return self;
}

- (void)initializeWithFilteredCategories:(NSSet*)filteredCategories {
    self.natureFilterSelected = [filteredCategories containsObject:kNatureCategory];
    self.friendsFilterSelected = [filteredCategories containsObject:kFriendsCategory];
    self.defaultFilterSelected = [filteredCategories containsObject:kDefaultCategory];
    
    @weakify(self)
    self.executeDoneButton = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self)
        return [self doneSignal];
    }];
}

- (RACSignal*)doneSignal {
    @weakify(self)
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        NSMutableArray *filteredCategories = [NSMutableArray arrayWithCapacity:3];
        [self filteredCategories:filteredCategories addCategory:kNatureCategory filtered:self.natureFilterSelected];
        [self filteredCategories:filteredCategories addCategory:kFriendsCategory filtered:self.friendsFilterSelected];
        [self filteredCategories:filteredCategories addCategory:kDefaultCategory filtered:self.defaultFilterSelected];
        [subscriber sendNext:[NSSet setWithArray:filteredCategories]];
        [subscriber sendCompleted];
        
        return nil;
    }];
}

- (void)filteredCategories:(NSMutableArray*)filteredCategories addCategory:(NSString*)category filtered:(BOOL)filtered {
    if (filtered) {
        [filteredCategories addObject:category];
    }
}

@end
