//
//  SignUpViewModel.h
//  PhotoMap
//
//  Created by mac-251 on 2/12/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PFUser;
@class RACCommand;
@class RACSignal;

@interface PMSignUpViewModel : NSObject

@property (strong, nonatomic) PFUser *potentialUser;
@property (strong, nonatomic) NSString *confirmPasswordText;

@property (strong, nonatomic) RACCommand *executeSignUp;
@property (strong, nonatomic) RACSignal *validUsernameSignal;
@property (strong, nonatomic) RACSignal *validEmailSignal;
@property (strong, nonatomic) RACSignal *validPasswordSignal;
@property (strong, nonatomic) RACSignal *validConfirmPasswordSignal;

@end
