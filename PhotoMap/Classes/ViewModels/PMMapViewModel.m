//
//  PMMapViewModel.m
//  PhotoMap
//
//  Created by mac-251 on 2/24/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMMapViewModel.h"
#import "PMPhoto.h"
#import "PMHashtag.h"
#import "PMPhotoIndex.h"
#import "PMUtils.h"
#import "PMPhotoAnnotation.h"
#import "PMDateManager.h"
#import "AppDelegate.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

CGFloat const kMaxThumbnailSizeLength = 100.f;

@interface PMMapViewModel ()

@property (strong, nonatomic) NSData *photoThumbnailData;
@property (strong, nonatomic) NSData *photoData;

@end

@implementation PMMapViewModel

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    self.locationManager = [CLLocationManager new];
    self.photos = [NSMutableArray array];
    self.photoLocation = nil;
    self.photoCreatedDate = nil;
    self.photoData = nil;
    self.photoThumbnailData = nil;
    self.photoDescriptionText = @"";
    self.categories = @[kDefaultCategory, kNatureCategory, kFriendsCategory];
    self.selectedCategory = kDefaultCategory;
    
    @weakify(self)
    
    self.executeLoadPhotos = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self)
        return [self loadPhotosSignal];
    }];
    
    self.executeCreatePhoto = [[RACCommand alloc] initWithEnabled:[[[AppDelegate sharedDelegate] reachableSignal] takeUntil:[self rac_willDeallocSignal]] signalBlock:^RACSignal *(id input) {
        @strongify(self)
        return [self createIndexedPhotoSignal];
    }];
    
    self.attemptToCreatePhotoSignal = [RACObserve(self, photoLocation) filter:^BOOL(CLLocation *newLocation) {
        return newLocation != nil;
    }];
    self.filterPhotosSignal = [RACObserve(self, filteredCategories) map:^NSArray*(NSSet *filteredCategories) {
        @strongify(self)
        NSMutableArray *annotations = [NSMutableArray array];
        for (PMPhoto *photo in self.photos) {
            if ([filteredCategories containsObject:photo.category]) {
                [annotations addObject:[[PMPhotoAnnotation alloc] initWithPhoto:photo]];
            }
        }
        return annotations;
    }];
}

- (void)setPhotoDataWithImage:(UIImage*)originalImage {
    UIImage *thumbnailImage = [PMUtils resizeImage:originalImage maxImageSizeLength:kMaxThumbnailSizeLength];
    self.photoData = UIImageJPEGRepresentation(originalImage, 0.5f);
    self.photoThumbnailData = UIImageJPEGRepresentation(thumbnailImage, 0.5f);
}

- (PMPhoto*)photoWithPhotoData {
    PMPhoto *photo = [PMPhoto photoWithData:self.photoData thumbnailData:self.photoThumbnailData createdTime:self.photoCreatedDate category:self.selectedCategory descriptionText:self.photoDescriptionText latitude:self.photoLocation.coordinate.latitude longitude:self.photoLocation.coordinate.longitude owner:[PFUser currentUser] error:nil];
    return photo;
}

- (NSString*)formatPhotoCreatedDate {
    return [NSString stringWithFormat:[[PMDateManager defaultDateManager].createdPhotoDateFormatter stringFromDate:self.photoCreatedDate], [PMDateManager daySuffixForDate:self.photoCreatedDate]];
}

- (void)requestUserLocationAuthorization {
    [self.locationManager requestWhenInUseAuthorization];
}

- (void)clearPhotoData {
    self.photoLocation = nil;
    self.photoData = nil;
    self.photoThumbnailData = nil;
    self.photoCreatedDate = nil;
    self.selectedCategory = kDefaultCategory;
    self.photoDescriptionText = @"";
}

- (RACSignal*)loadPhotosSignal {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        PFQuery *query = [PMPhoto query];
        NSString *ownerKey = NSStringFromSelector(@selector(owner));
        [query whereKey:ownerKey equalTo:[PFUser currentUser]];
        @weakify(self)
        [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
            @strongify(self)
            if (error) {
                [subscriber sendError:error];
            } else {
                [self.photos removeAllObjects];
                [self.photos addObjectsFromArray:objects];
                self.filteredCategories = [NSSet setWithArray:self.categories];
                [subscriber sendNext:objects];
                [subscriber sendCompleted];
            }
        }];
        
        return nil;
    }];
}

- (RACSignal*)createIndexedPhotoSignal {
    NSArray *photoHashtags = [PMHashtag parseTagsInText:self.photoDescriptionText];
    if (photoHashtags.count == 0) {
        return [self createPhotoSignalAndClearPhotoData:YES];
    } else {
        @weakify(self)
        return [[[self createPhotoSignalAndClearPhotoData:NO]
                  then:^RACSignal *{
                      @strongify(self)
                      return [self findAllHashtagsSignal];
                  }] flattenMap:^RACStream *(NSArray *allHashtagObjects) {
                      @strongify(self)
                      RACSignal *newPhotoIndexesSignal = [RACSignal empty];
                      for (NSString* tag in photoHashtags) {
                          PMHashtag* hashtag = [self hashtags:allHashtagObjects findHashtagWithTag:tag];
                          if (hashtag) {
                              @weakify(self)
                              newPhotoIndexesSignal = [newPhotoIndexesSignal then:^RACSignal *{
                                  @strongify(self)
                                  return [self createPhotoIndexSignalWithPhoto:[self.photos lastObject] hashtag:hashtag];
                              }];
                          } else {
                              @weakify(self)
                              newPhotoIndexesSignal = [newPhotoIndexesSignal then:^RACSignal *{
                                  @strongify(self)
                                  return [self indexPhotoSignalWithTag:tag];
                              }];
                          }
                      }
                      [self clearPhotoData];
                      return newPhotoIndexesSignal;
                  }];
    }
}

- (PMHashtag*)hashtags:(NSArray*)hashtags findHashtagWithTag:(NSString*)tag {
    for (PMHashtag* hashtag in hashtags) {
        if ([hashtag.hashtag isEqualToString:tag]) {
            return hashtag;
        }
    }
    
    return nil;
}

- (RACSignal*)indexPhotoSignalWithTag:(NSString*)tag {
    @weakify(self)
    return [[self createHashtagSignalWithTag:tag] flattenMap:^RACStream *(PMHashtag *hashtag) {
        @strongify(self)
        return [self createPhotoIndexSignalWithPhoto:[self.photos lastObject] hashtag:hashtag];
    }];
}

- (RACSignal*)createPhotoSignalAndClearPhotoData:(BOOL)clearPhotoData {
    @weakify(self)
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        NSError *error = nil;
        PMPhoto *photo = [PMPhoto photoWithData:self.photoData thumbnailData:self.photoThumbnailData createdTime:self.photoCreatedDate category:self.selectedCategory descriptionText:self.photoDescriptionText latitude:self.photoLocation.coordinate.latitude longitude:self.photoLocation.coordinate.longitude owner:[PFUser currentUser] error:&error];
        if (error) {
            [subscriber sendError:error];
        } else {
            @weakify(self)
            [photo saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                @strongify(self)
                if (clearPhotoData) {
                    [self clearPhotoData];
                }
                if (succeeded) {
                    [self.photos addObject:photo];
                    [subscriber sendNext:[[PMPhotoAnnotation alloc] initWithPhoto:photo]];
                    [subscriber sendCompleted];
                } else {
                    [subscriber sendError:error];
                }
            }];
        }
        
        return nil;
    }];
}

- (RACSignal*)findAllHashtagsSignal {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [[PMHashtag query] findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
            if (error) {
                [subscriber sendError:error];
            } else {
                [subscriber sendNext:objects];
                [subscriber sendCompleted];
            }
        }];
        
        return nil;
    }];
}

- (RACSignal*)createPhotoIndexSignalWithPhoto:(PMPhoto*)photo hashtag:(PMHashtag*)hashtag {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [[PMPhotoIndex photoIndexWithPhoto:photo hashtag:hashtag] saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
            if (succeeded) {
                [subscriber sendNext:[[PMPhotoAnnotation alloc] initWithPhoto:photo]];
                [subscriber sendCompleted];
            } else {
                [subscriber sendError:error];
            }
        }];
        return nil;
    }];
}

- (RACSignal*)createHashtagSignalWithTag:(NSString*)tag {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        PMHashtag *hashtag = [PMHashtag hashtagWithTag:tag];
        [hashtag saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
            if (succeeded) {
                [subscriber sendNext:hashtag];
                [subscriber sendCompleted];
            } else {
                [subscriber sendError:error];
            }
        }];
        return nil;
    }];
}

- (RACSignal*)loadPhotoThumbnailDataSignalWithPhoto:(PMPhoto*)photo takeUntil:(RACSignal*)takeUntilSignal {
    return [photo loadPhotoThumbnailDataSignalTakeUntil:takeUntilSignal];
}

@end
