//
//  PMTimelineViewModel.h
//  PhotoMap
//
//  Created by mac-251 on 3/22/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACCommand;
@class RACSignal;
@class PMPhoto;

@interface PMTimelineViewModel : NSObject

@property (strong, nonatomic) NSString *searchText;
@property (strong, nonatomic) NSSet *filteredCategories;
@property (strong, nonatomic) NSArray *photos;
@property (strong, nonatomic) NSArray *photoSections;

@property (strong, nonatomic) RACCommand *executeSearchPhotos;

- (PMPhoto*)photoForIndexPath:(NSIndexPath*)indexPath;

@end
