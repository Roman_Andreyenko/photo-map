//
//  PMFullPhotoViewModel.h
//  PhotoMap
//
//  Created by mac-251 on 3/30/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PMPhoto;
@class RACSignal;
@class RACCommand;

@interface PMFullPhotoViewModel : NSObject

- (instancetype)initWithPhoto:(PMPhoto*)photo;

- (NSString*)formatPhotoCreatedDate;

@property(nonatomic, strong) PMPhoto* photo;

@property(nonatomic, strong) RACCommand* executePhotoLoading;

@end
