//
//  PMMoreViewModel.h
//  PhotoMap
//
//  Created by mac-251 on 2/19/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACCommand;

@interface PMMoreViewModel : NSObject

@property (strong, nonatomic) RACCommand *executeLogOut;

@end
