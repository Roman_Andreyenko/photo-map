//
//  PMUtils.m
//  PhotoMap
//
//  Created by mac-251 on 2/19/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import "PMUtils.h"

@implementation PMUtils

+ (void)controller:(UIViewController*)controller showAlertWithMessage:(NSString*)message {
    [PMUtils controller:controller showAlertWithTitle:NSLocalizedString(@"Warning", @"") message:message];
}

+ (void)controller:(UIViewController*)controller showAlertWithTitle:(NSString*)title message:(NSString*)message {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [controller presentViewController:alert animated:YES completion:nil];
}

+ (UIImage*)resizeImage:(UIImage*)sourceImage maxImageSizeLength:(CGFloat)maxSizeLength {
    CGFloat scaleFactor = (sourceImage.size.width > sourceImage.size.height) ? maxSizeLength / sourceImage.size.width : maxSizeLength / sourceImage.size.height;
    CGSize newSize = CGSizeMake(truncf(sourceImage.size.width * scaleFactor), truncf(sourceImage.size.height * scaleFactor));
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [sourceImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (void)controller:(UIViewController*)controller showSettingsAlertWithMessage:(NSString*)message {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning", @"")
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    UIAlertAction* settingsAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Settings", @"") style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                                          }];
    
    [alert addAction:settingsAction];
    [controller presentViewController:alert animated:YES completion:nil];
}

@end
