//
//  PMUtils.h
//  PhotoMap
//
//  Created by mac-251 on 2/19/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PMUtils : NSObject

+ (void)controller:(UIViewController*)controller showAlertWithMessage:(NSString*)message;
+ (void)controller:(UIViewController*)controller showAlertWithTitle:(NSString*)title message:(NSString*)message;
+ (void)controller:(UIViewController*)controller showSettingsAlertWithMessage:(NSString*)message;

+ (UIImage*)resizeImage:(UIImage*)sourceImage maxImageSizeLength:(CGFloat)maxSizeLength;

@end
