//
//  PMDateManager.h
//  PhotoMap
//
//  Created by mac-251 on 3/30/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMDateManager : NSObject

+ (instancetype)defaultDateManager;
+ (NSString*)daySuffixForDate:(NSDate*)date;

@property (strong, nonatomic) NSDateFormatter *fullMonthYearDateFormatter;
@property (strong, nonatomic) NSDateFormatter *shortDateFormatter;
@property (strong, nonatomic) NSDateFormatter *createdPhotoDateFormatter;
@property (strong, nonatomic) NSDateFormatter *createdFullPhotoDateFormatter;

@end
