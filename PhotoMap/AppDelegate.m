//
//  AppDelegate.m
//  PhotoMap
//
//  Created by mac-251 on 12/17/15.
//  Copyright © 2015 mac-251. All rights reserved.
//

#import "AppDelegate.h"
#import "PMMapViewController.h"
#import "PMTimelineViewController.h"
#import "PMMoreViewController.h"
#import "PMWelcomeViewController.h"
#import "PMMoreViewModel.h"
#import "PMTimelineViewModel.h"
#import "PMMapViewModel.h"
#import "PMWelcomeViewModel.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

#import <Parse/Parse.h>

@interface AppDelegate ()

@property (nonatomic, strong) PMMapViewModel *mapViewModel;
@property (nonatomic, strong) PMTimelineViewModel *timelineViewModel;
@property (nonatomic, strong) PMMoreViewModel *moreViewModel;
@property (nonatomic, strong) PMWelcomeViewModel *welcomeViewModel;

- (UIViewController*)setupMainScreenViewController;
- (UIViewController*)setupInitScreenViewController;

@end

@implementation AppDelegate

+ (AppDelegate*)sharedDelegate {
    return [UIApplication sharedApplication].delegate;
}

- (RACSignal*)reachableSignal {
    @weakify(self)
    RACSignal* reachableSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        [subscriber sendNext:@([self.reachibility isReachable])];
        [subscriber sendCompleted];
        return nil;
    }];
    return [reachableSignal merge:[[[NSNotificationCenter defaultCenter] rac_addObserverForName:kGCNetworkReachabilityDidChangeNotification object:nil] map:^NSNumber*(NSNotification *notification) {
        GCNetworkReachabilityStatus status = [[notification userInfo][kGCNetworkReachabilityStatusKey] integerValue];
        return @(status != GCNetworkReachabilityStatusNotReachable);
    }]];
}

- (UIViewController*)setupInitScreenViewController {
    self.welcomeViewModel = [PMWelcomeViewModel new];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[[PMWelcomeViewController alloc] initWithViewModel:self.welcomeViewModel]];
    
    return navigationController;
}

- (UIViewController*)setupMainScreenViewController {
    self.mapViewModel = [PMMapViewModel new];
    PMMapViewController *mapViewController = [[PMMapViewController alloc] initWithViewModel:self.mapViewModel];
    mapViewController.title = NSLocalizedString(@"Map", @"");
    mapViewController.tabBarItem.image = [UIImage imageNamed:@"MapIcon.png"];
    
    self.timelineViewModel = [PMTimelineViewModel new];
    PMTimelineViewController *timelineViewController = [[PMTimelineViewController alloc] initWithViewModel:self.timelineViewModel];
    UINavigationController *timelineTabNavigationController = [[UINavigationController alloc] initWithRootViewController:timelineViewController];
    timelineTabNavigationController.title = NSLocalizedString(@"Timeline", @"");
    timelineTabNavigationController.tabBarItem.image = [UIImage imageNamed:@"TimelineIcon.png"];
    
    self.moreViewModel = [PMMoreViewModel new];
    PMMoreViewController *moreViewController = [[PMMoreViewController alloc] initWithViewModel:self.moreViewModel];
    moreViewController.title = NSLocalizedString(@"More", @"");
    moreViewController.tabBarItem.image = [UIImage imageNamed:@"MoreIcon.png"];
    
    UITabBarController *tabBarController = [UITabBarController new];
    tabBarController.viewControllers = @[mapViewController, timelineTabNavigationController, moreViewController];
    
    return tabBarController;
}

- (void)setupRootViewControllerAnimated:(BOOL)animated {
    self.window.rootViewController = [PFUser currentUser] ? [self setupMainScreenViewController] : [self setupInitScreenViewController];
    [self.window makeKeyAndVisible];
    
    if (animated) {
        self.window.rootViewController.view.alpha = 0.0;
        
        [UIView animateWithDuration:1.0 animations:^{
            self.window.rootViewController.view.alpha = 1.0;
        }];
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.reachibility = [GCNetworkReachability reachabilityForInternetConnection];
    [self.reachibility startMonitoringNetworkReachabilityWithNotification];
    
    [Parse setApplicationId:@"Z6IJsvc8jNSxZ8rMh2YzybiYvKlEOc8ljCetKNrv"
                  clientKey:@"FyN2Im0vEPh75wTLWCQenPYBJxa1Vh4aEgfMWvhx"];
    
    [[UIPickerView appearance] setBackgroundColor:[UIColor lightGrayColor]];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    [self setupRootViewControllerAnimated:NO];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self.reachibility stopMonitoringNetworkReachability];
}

@end
