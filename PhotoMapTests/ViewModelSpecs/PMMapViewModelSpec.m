//
//  PMMapViewModelSpec.m
//  PhotoMap
//
//  Created by mac-251 on 4/7/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <Kiwi/Kiwi.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "PMMapViewModel.h"
#import "PMPhotoAnnotation.h"
#import "PMPhotoIndex.h"
#import "PMHashtag.h"
#import "PMPhoto.h"

SPEC_BEGIN(PMMapViewModelSpec)

describe(@"PMMapViewModel", ^{
    
    const double TIMEOUT = 60.f;
    
    __block PMMapViewModel* viewModel;
    
    beforeEach(^{
        viewModel = [PMMapViewModel new];
    });
    
    context(@"when newly created", ^{
        
        specify(^{
            [[viewModel.locationManager shouldNot] beNil];
            [[viewModel.photos shouldNot] beNil];
            [[viewModel.photoLocation should] beNil];
            [[viewModel.photoCreatedDate should] beNil];
            [[viewModel.photoDescriptionText should] equal:@""];
            [[viewModel.categories should] haveCountOf:3];
            [[viewModel.categories should] containObjectsInArray:@[kDefaultCategory, kNatureCategory, kFriendsCategory]];
            [[viewModel.selectedCategory should] equal:kDefaultCategory];
            [[viewModel.executeLoadPhotos shouldNot] beNil];
            [[viewModel.executeCreatePhoto shouldNot] beNil];
            [[viewModel.attemptToCreatePhotoSignal shouldNot] beNil];
            [[viewModel.filterPhotosSignal shouldNot] beNil];
        });
        
        if ([PFUser currentUser]) {
            it(@"should load available photos for current user and photo data should be loaded", ^{
                __block NSArray* userPhotos = nil;
                __block UIImage* imageData = nil;
                [[viewModel.executeLoadPhotos.executionSignals switchToLatest] subscribeNext:^(id next) {
                    [[next should] beNonNil];
                    [[next should] beKindOfClass:[NSArray class]];
                    NSArray *photos = next;
                    for (id object in photos) {
                        [[object should] beKindOfClass:[PMPhoto class]];
                        PMPhoto *photo = object;
                        [[photo.owner.objectId should] equal:[PFUser currentUser].objectId];
                    }
                    userPhotos = photos;
                    if (photos.count > 0) {
                        [[viewModel loadPhotoThumbnailDataSignalWithPhoto:[photos lastObject] takeUntil:nil] subscribeNext:^(id x) {
                            [[x should] beNonNil];
                            [[x should] beKindOfClass:[UIImage class]];
                            imageData = x;
                        } error:^(NSError *error) {
                            [[error should] beNil];
                        }];
                    }
                }];
                [viewModel.executeLoadPhotos.errors subscribeNext:^(NSError *error) {
                    [[error should] beNil];
                }];
                [viewModel.executeLoadPhotos execute:nil];
                [[expectFutureValue(userPhotos) shouldEventuallyBeforeTimingOutAfter(TIMEOUT)] beNonNil];
                [[expectFutureValue(imageData) shouldEventuallyBeforeTimingOutAfter(TIMEOUT)] beNonNil];
            });
        }
        
    });
    
    context(@"when filtered categories were changed", ^{
        it(@"should filter photo annotations", ^{
            [viewModel.filterPhotosSignal subscribeNext:^(id next) {
                [[next should] beNonNil];
                [[next should] beKindOfClass:[NSArray class]];
                NSArray *annotations = next;
                for (id object in annotations) {
                    [[object should] beKindOfClass:[PMPhotoAnnotation class]];
                    PMPhotoAnnotation *annotation = object;
                    [[annotation.photo shouldNot] beNil];
                    [[viewModel.filteredCategories should] contain:annotation.photo.category];
                }
            }];
            viewModel.filteredCategories = [NSSet setWithObjects:kNatureCategory, kDefaultCategory, kFriendsCategory, nil];
        });
    });
    
    context(@"when photo data was cleared", ^{
        
        it(@"should has clear data", ^{
            [viewModel clearPhotoData];
            [[viewModel.photoLocation should] beNil];
            [[viewModel.photoCreatedDate should] beNil];
            [[viewModel.selectedCategory should] equal:kDefaultCategory];
            [[viewModel.photoDescriptionText should] equal:@""];
        });
        
    });
    
    context(@"when photo was created", ^{
        
        __block NSDateFormatter* dateFormatter = [NSDateFormatter new];
        
        beforeAll(^{
            dateFormatter.dateFormat = @"dd/MM/yyyy HH:mm";
        });
        
        void (^checkCreatedDateFormat)(NSString*, NSString*) = ^(NSString* initialCreatedDateText, NSString* expectedCreatedDateText) {
            
            context([@"and setup created date to " stringByAppendingString:initialCreatedDateText], ^{
                
                it([@"should be equal to " stringByAppendingString:expectedCreatedDateText], ^{
                    viewModel.photoCreatedDate = [dateFormatter dateFromString:initialCreatedDateText];
                    [[[viewModel formatPhotoCreatedDate] should] equal:expectedCreatedDateText];
                });
            });
        };
        
        NSString *initialCreatedDateTextCase1 = @"01/04/2015 13:33";
        NSString *expectedCreatedDateTextCase1 = @"April 1st, 2015 - 1:33 pm";
        checkCreatedDateFormat(initialCreatedDateTextCase1, expectedCreatedDateTextCase1);
        
        NSString *initialCreatedDateTextCase2 = @"02/04/2015 12:33";
        NSString *expectedCreatedDateTextCase2 = @"April 2nd, 2015 - 12:33 pm";
        checkCreatedDateFormat(initialCreatedDateTextCase2, expectedCreatedDateTextCase2);

        NSString *initialCreatedDateTextCase3 = @"03/04/2015 11:33";
        NSString *expectedCreatedDateTextCase3 = @"April 3rd, 2015 - 11:33 am";
        checkCreatedDateFormat(initialCreatedDateTextCase3, expectedCreatedDateTextCase3);

        NSString *initialCreatedDateTextCase4 = @"04/04/2015 00:00";
        NSString *expectedCreatedDateTextCase4 = @"April 4th, 2015 - 12:00 am";
        checkCreatedDateFormat(initialCreatedDateTextCase4, expectedCreatedDateTextCase4);
        
    });
    
    context(@"when photo data was filled", ^{
        
        NSDate* photoCreatedDate = [NSDate new];
        NSString* selectedCategory = kDefaultCategory;
        CLLocation* location = [[CLLocation alloc] initWithLatitude:57. longitude:57.];
        
        beforeEach(^{
            [viewModel setPhotoDataWithImage:[UIImage imageNamed:@"PlaceholderIcon"]];
            viewModel.photoCreatedDate = photoCreatedDate;
            viewModel.selectedCategory = selectedCategory;
            viewModel.photoLocation = location;
        });
        
        it(@"photo should be created correctly", ^{
            viewModel.photoDescriptionText = @"desc";
            PMPhoto *photo = [viewModel photoWithPhotoData];
            [[photo shouldNot] beNil];
            [[photo.createdTime should] equal:photoCreatedDate];
            [[photo.descriptionText should] equal:@"desc"];
            [[photo.category should] equal:selectedCategory];
            [[photo.latitude should] equal:[NSNumber numberWithDouble:location.coordinate.latitude]];
            [[photo.longitude should] equal:[NSNumber numberWithDouble:location.coordinate.longitude]];
        });
        
        if ([PFUser currentUser]) {
            
            NSString *descriptionTextCase1 = @"";
            
            context(@"with description text without hashtags", ^{
                
                it(@"should create photo without hashtags", ^{
                    __block PMPhoto* createdPhoto = nil;
                    __block PMPhoto* foundPhoto = nil;
                    __block BOOL deletedPhoto = NO;
                    [[viewModel.executeCreatePhoto.executionSignals switchToLatest] subscribeNext:^(id next) {
                        [[next shouldNot] beNil];
                        [[next should] beKindOfClass:[PMPhotoAnnotation class]];
                        PMPhotoAnnotation* photoAnnotation = next;
                        [[photoAnnotation.photo shouldNot] beNil];
                        createdPhoto = photoAnnotation.photo;
                        [[createdPhoto.owner should] equal:[PFUser currentUser]];
                        [[createdPhoto.createdTime should] equal:photoCreatedDate];
                        [[createdPhoto.category should] equal:selectedCategory];
                        [[createdPhoto.descriptionText should] equal:descriptionTextCase1];
                        [[createdPhoto.latitude should] equal:[NSNumber numberWithDouble:location.coordinate.latitude]];
                        [[createdPhoto.longitude should] equal:[NSNumber numberWithDouble:location.coordinate.longitude]];
                        [[PMPhoto query] getObjectInBackgroundWithId:photoAnnotation.photo.objectId block:^(PFObject * _Nullable object, NSError * _Nullable error) {
                            [[error should] beNil];
                            [[object shouldNot] beNil];
                            [[object should] beKindOfClass:[PMPhoto class]];
                            foundPhoto = (PMPhoto*)object;
                            [foundPhoto deleteInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                                [[error should] beNil];
                                deletedPhoto = succeeded;
                            }];
                        }];
                    }];
                    [viewModel.executeCreatePhoto.errors subscribeNext:^(NSError *error) {
                        [[error should] beNil];
                    }];
                    viewModel.photoDescriptionText = descriptionTextCase1;
                    [viewModel.executeCreatePhoto execute:nil];
                    [[expectFutureValue(createdPhoto) shouldEventuallyBeforeTimingOutAfter(TIMEOUT)] beNonNil];
                    [[expectFutureValue(foundPhoto) shouldEventuallyBeforeTimingOutAfter(TIMEOUT)] beNonNil];
                    [[expectFutureValue(theValue(deletedPhoto)) shouldEventuallyBeforeTimingOutAfter(TIMEOUT)] beYes];
                });
                
            });
            
            NSString* appleHashtag = @"#apple";
            NSString* tableHashtag = @"#table";
            NSString* descriptionTextCase2 = [NSString stringWithFormat:@"%@ %@", appleHashtag, tableHashtag];
            
            context([@"with description text with hashtags: " stringByAppendingString:descriptionTextCase2], ^{
                
                it([NSString stringWithFormat:@"should create photo with hashtags: %@ %@", appleHashtag, tableHashtag], ^{
                    __block PMPhoto* createdPhoto = nil;
                    __block PMPhoto* foundPhoto = nil;
                    __block BOOL deletedPhoto = NO;
                    __block NSArray* hashtags = nil;
                    [[viewModel.executeCreatePhoto.executionSignals switchToLatest] subscribeNext:^(id next) {
                        [[next shouldNot] beNil];
                        [[next should] beKindOfClass:[PMPhotoAnnotation class]];
                        PMPhotoAnnotation* photoAnnotation = next;
                        [[photoAnnotation.photo shouldNot] beNil];
                        createdPhoto = photoAnnotation.photo;
                        [[createdPhoto.owner should] equal:[PFUser currentUser]];
                        [[createdPhoto.createdTime should] equal:photoCreatedDate];
                        [[createdPhoto.category should] equal:selectedCategory];
                        [[createdPhoto.descriptionText should] equal:descriptionTextCase2];
                        [[createdPhoto.latitude should] equal:[NSNumber numberWithDouble:location.coordinate.latitude]];
                        [[createdPhoto.longitude should] equal:[NSNumber numberWithDouble:location.coordinate.longitude]];
                        PFQuery* query = [PMPhotoIndex query];
                        NSString* photoKey = NSStringFromSelector(@selector(photo));
                        [query whereKey:photoKey equalTo:createdPhoto];
                        [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
                            [[error should] beNil];
                            [[objects shouldNot] beNil];
                            [[objects should] haveCountOf:2];
                            for (PMPhotoIndex* index in objects) {
                                NSError* error = nil;
                                [[theValue([index delete:&error]) should] beYes];
                                [[error should] beNil];
                            }
                            [[PMPhoto query] getObjectInBackgroundWithId:photoAnnotation.photo.objectId block:^(PFObject * _Nullable object, NSError * _Nullable error) {
                                [[error should] beNil];
                                [[object shouldNot] beNil];
                                [[object should] beKindOfClass:[PMPhoto class]];
                                foundPhoto = (PMPhoto*)object;
                                [foundPhoto deleteInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                                    [[error should] beNil];
                                    deletedPhoto = succeeded;
                                }];
                            }];
                        }];
                        PFQuery* hashtagQuery = [PMHashtag query];
                        NSString* hashtag = NSStringFromSelector(@selector(hashtag));
                        NSArray* hashtagStrings = @[[appleHashtag stringByReplacingOccurrencesOfString:@"#" withString:@""], [tableHashtag stringByReplacingOccurrencesOfString:@"#" withString:@""]];
                        [hashtagQuery whereKey:hashtag containedIn:hashtagStrings];
                        [hashtagQuery findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
                            [[error should] beNil];
                            [[objects shouldNot] beNil];
                            [[objects should] haveCountOf:2];
                            hashtags = objects;
                        }];
                    }];
                    [viewModel.executeCreatePhoto.errors subscribeNext:^(NSError *error) {
                        [[error should] beNil];
                    }];
                    viewModel.photoDescriptionText = descriptionTextCase2;
                    [viewModel.executeCreatePhoto execute:nil];
                    [[expectFutureValue(createdPhoto) shouldEventuallyBeforeTimingOutAfter(TIMEOUT)] beNonNil];
                    [[expectFutureValue(foundPhoto) shouldEventuallyBeforeTimingOutAfter(TIMEOUT)] beNonNil];
                    [[expectFutureValue(theValue(deletedPhoto)) shouldEventuallyBeforeTimingOutAfter(TIMEOUT)] beYes];
                    [[expectFutureValue(theValue(hashtags)) shouldEventuallyBeforeTimingOutAfter(TIMEOUT)] beNonNil];
                });
            });
        }
    });
});

SPEC_END
