//
//  PMTimelineViewModelSpec.m
//  PhotoMap
//
//  Created by mac-251 on 4/22/16.
//  Copyright © 2016 mac-251. All rights reserved.
//

#import <Kiwi/Kiwi.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "PMTimelineViewModel.h"
#import "PMPhoto.h"
#import "PMPhotoIndex.h"
#import "PMHashtag.h"

SPEC_BEGIN(PMTimelineViewModelSpec)

describe(@"PMTimelineViewModel", ^{
    
    const double TIMEOUT = 60.f;
    
    __block PMTimelineViewModel* viewModel;
    
    beforeEach(^{
        viewModel = [PMTimelineViewModel new];
    });
    
    context(@"when newly created", ^{
        
        specify(^{
            [[viewModel.searchText should] equal:@""];
            [[viewModel.filteredCategories should] haveCountOf:3];
            [[viewModel.filteredCategories should] containObjectsInArray:@[kDefaultCategory, kNatureCategory, kFriendsCategory]];
            [[viewModel.photos shouldNot] beNil];
            [[viewModel.photos should] beEmpty];
            [[viewModel.photoSections shouldNot] beNil];
            [[viewModel.photoSections should] beEmpty];
            [[viewModel.executeSearchPhotos shouldNot] beNil];
        });
    });
    
    void (^searchBlock)(NSString *, NSSet *, NSArray *) = ^(NSString *searchText, NSSet *filteredCategories, NSArray *hashtags) {
        it([@"should search photos correctly with search text: " stringByAppendingString:searchText], ^{
            __block NSArray *userPhotos = nil;
            [[viewModel.executeSearchPhotos.executionSignals switchToLatest] subscribeNext:^(id next) {
                [[next should] beNonNil];
                [[next should] beKindOfClass:[NSArray class]];
                NSArray *photos = next;
                for (id object in photos) {
                    [[object should] beKindOfClass:[PMPhoto class]];
                    PMPhoto *photo = object;
                    [[photo.owner.objectId should] equal:[PFUser currentUser].objectId];
                    [[filteredCategories should] contain:photo.category];
                    if (hashtags.count > 0) {
                        BOOL hashtagWasFound = NO;
                        for (NSString* tag in hashtags) {
                            if ([photo.descriptionText rangeOfString:[@"#" stringByAppendingString:tag]].location != NSNotFound) {
                                hashtagWasFound = YES;
                                break;
                            }
                        }
                        [[theValue(hashtagWasFound) should] beYes];
                    }
                }
                userPhotos = photos;
            }];
            [viewModel.executeSearchPhotos.errors subscribeNext:^(NSError *error) {
                [[error should] beNil];
            }];
            viewModel.searchText = searchText;
            viewModel.filteredCategories = filteredCategories;
            [viewModel.executeSearchPhotos execute:nil];
            [[expectFutureValue(userPhotos) shouldEventuallyBeforeTimingOutAfter(TIMEOUT)] beNonNil];
        });
    };
    
    searchBlock(@"#apple #table", [NSSet setWithArray:@[kDefaultCategory, kFriendsCategory]], @[@"apple", @"table"]);
    searchBlock(@"apple #table", [NSSet setWithArray:@[kDefaultCategory, kFriendsCategory]], @[@"table"]);
    searchBlock(@"apple table", [NSSet setWithArray:@[kDefaultCategory, kFriendsCategory]], @[]);
    searchBlock(@"", [NSSet setWithArray:@[kDefaultCategory]], @[]);
    searchBlock(@"", [NSSet setWithArray:@[]], @[]);
});

SPEC_END